﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JChat
{
    /// <summary>
    /// Window form to display a user´s statistics
    /// </summary>
    public partial class UserStatisticsWindow : Form
    {
        Form parent;
        int userId;
        Data.User user = null;
        Models.ServerApp serverApp = Models.ServerApp.Instance();

        /// <summary>
        /// Construct
        /// </summary>
        /// <param name="parent">Parent window form object</param>
        /// <param name="userId">User´s identification number</param>
        public UserStatisticsWindow(Form parent, int userId)
        {
            this.parent = parent;
            this.userId = userId;
            parent.Hide();

            user = this.serverApp.getUser(userId);

            if (user == null)
            {
                MessageBox.Show(this, "occurred an error while getting user´s information...", "Error...");
                this.parent.Show();
                this.Dispose();
            }

            InitializeComponent();

            this.Text = "JChat - " + user.firstName + " " + user.lastName + "´s statistics";

            String lastLogin = this.serverApp.getUserLastLogin(this.user.id);
            int? numberOfLogins = this.serverApp.getNumberOfTimeUserLogged(this.user.id);
            int? numberOfFriends = this.serverApp.getUserNumberOfFriends(this.user.id);

            if (lastLogin == null || numberOfFriends == null || numberOfLogins == null)
            {
                MessageBox.Show(this, "occurred an error while getting user´s statistics...", "Error...");
                this.parent.Show();
                this.Dispose();
            }

            label4.Text = lastLogin;
            label5.Text = Convert.ToString(numberOfLogins);
            label6.Text = Convert.ToString(numberOfFriends);
        }

        /// <summary>
        /// Window form closed´s event handler to close current window form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserStatisticsWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.parent.Show();
            this.Dispose();
        }

        /// <summary>
        /// Button click´s evenet handler to close current window form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.parent.Show();
            this.Dispose();
        }
    }
}
