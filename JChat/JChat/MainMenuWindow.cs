﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JChat
{
    /// <summary>
    /// Main window form for the chat server
    /// </summary>
    public partial class MainMenuWindow : Form
    {
        Data.ServerSockets.AsynchronousSocketListener server = null;
        Boolean serverStarted = false;

        /// <summary>
        /// Constructor
        /// </summary>
        public MainMenuWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Button click´s event handler to display window form with all users registerd in database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            ManageUsersWindow manageUsers = new ManageUsersWindow(this);
        }

        /// <summary>
        /// Button click´s event handler to start server to listening clients requests
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (!this.serverStarted)
            {
                this.server = new Data.ServerSockets.AsynchronousSocketListener();
                Task t = Task.Factory.StartNew(() => this.server.StartServer());
                button2.Text = "Stop server...";
                this.serverStarted = true;
            }
            else
            {
                this.server.ShutDownServer();
                this.server = null;
                GC.Collect();
                button2.Text = "Start server...";
                this.serverStarted = false;
            }
        }
    }
}
