﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace JChat.Data
{
    /// <summary>
    /// Entity framework ORM
    /// </summary>
    class JChatDB : DbContext
    {

        public JChatDB()
            : base("name=JChatDB")
        {
            Database.SetInitializer<JChatDB>(new CreateDatabaseIfNotExists<JChatDB>());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<OnlineStatus> OnlineStatus { get; set; }
        public DbSet<FriendshipRequest> FriendshipRequests { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            //Configure domain classes using Fluent API here

            //modelBuilder.Entity<City>()
            //.HasMany(p => p.proposals)
            //.WithRequired(c => c.city)
            //.WillCascadeOnDelete(true);

            //modelBuilder.Entity<City>()
            //    .HasMany<Proposal>(c => c.cityProposals)
            //    .WithOptional(x => x.city)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();


        }
    }

    /* TABLE MODELS */
    [Table("USERS")]
    public class User
    {

        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NICKNAME")]
        [StringLength(20)]
        public String nickname { get; set; }

        [Column("FIRST_NAME")]
        [StringLength(100)]
        public String firstName { get; set; }

        [Column("LAST_NAME")]
        [StringLength(100)]
        public String lastName { get; set; }

        [Column("PASSWORD")]
        [StringLength(20, MinimumLength = 8)]
        public String password { get; set; }

        [Column("EMAIL")]
        public String email { get; set; }

        [Column("CITY_ID")]
        public int? cityId { get; set; }

        [Column("GENDER_ID")]
        public int genderId { get; set; }

        [Column("BIRTH_DATE")]
        public DateTime birthDate { get; set; }

        [Column("ENABLED")]
        public Boolean enabled { get; set; }

        [Column("ONLINE_STATUS_ID")]
        [ForeignKey("onlineStatus")]
        public int onlineStatusId { get; set; }

        [Column("REGISTER_DATE")]
        public DateTime registerDate { get; set; }

        [Column("LOGGED")]
        public Boolean logged { get; set; }

        // Fks
        [InverseProperty("sender")]
        public virtual List<Message> messagesSent { get; set; }

        [InverseProperty("receiver")]
        public virtual List<Message> messagesReceived { get; set; }

        [InverseProperty("user")]
        public virtual List<Contact> userInContacts { get; set; }

        [InverseProperty("contact")]
        public virtual List<Contact> contacts { get; set; }

        public virtual List<Session> sessions { get; set; }

        public virtual OnlineStatus onlineStatus { get; set; }

        [InverseProperty("requester")]
        public virtual List<FriendshipRequest> friendshipRequestsMade { get; set; }

        [InverseProperty("requested")]
        public virtual List<FriendshipRequest> frinedshipUsersRequested { get; set; }
    }

    [Table("SESSIONS")]
    public class Session
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("USER_ID")]
        [ForeignKey("user")]
        public int userId { get; set; }

        [Column("LOGIN_DATE_TIME")]
        public DateTime loginDateTime { get; set; }

        [Column("LOGOUT_DATE_TIME")]
        public DateTime? logoutDateTime { get; set; }

        // Fks
        public virtual User user { get; set; }
    }

    [Table("GENDERS")]
    public class Gender
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("DESCRIPTION")]
        public String description { get; set; }
    }

    [Table("COUNTRIES")]
    public class Country
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        public String name { get; set; }
    }

    [Table("CITIES")]
    public class City
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("NAME")]
        public String name { get; set; }

        [Column("COUNTRY_ID")]
        [ForeignKey("country")]
        public int countryId { get; set; }

        // FKs
        public virtual Country country { get; set; }
    }

    [Table("MESSAGES")]
    public class Message
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("SENDER_ID")]
        [ForeignKey("sender")]
        public int senderId { get; set; }

        [Column("RECEIVER_ID")]
        [ForeignKey("receiver")]
        public int receiverId { get; set; }

        [Column("DATE_TIME")]
        public DateTime dateTime { get; set; }

        [Column("MESSAGE")]
        public String messsage { get; set; }

        // Fks
        [InverseProperty("messagesSent")]
        public virtual User sender { get; set; }

        [InverseProperty("messagesReceived")]
        public virtual User receiver { get; set; }
    }

    [Table("CONTACTS")]
    public class Contact
    {
        [Key]
        [Column("USER_ID", Order = 1)]
        public int userId { get; set; }

        [Key]
        [Column("CONTACT_ID", Order = 2)]
        public int contactId { get; set; }

        [Column("CONNECTION_DATE_TIME")]
        public DateTime connetionDateTime { get; set; }

        [Column("BLOCKED")]
        public Boolean blocked { get; set; }

        // Fks
        [ForeignKey("userId")]
        [InverseProperty("userInContacts")]
        public virtual User user { get; set; }

        [ForeignKey("contactId")]
        [InverseProperty("contacts")]
        public virtual User contact { get; set; }
    }

    [Table("ONLINE_STATUS")]
    public class OnlineStatus
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("DESCRIPTION")]
        public String description { get; set; }
    }

    [Table("FRIENDSHIP_REQUESTS")]
    public class FriendshipRequest
    {
        [Key]
        [Column("ID")]
        public int id { get; set; }

        [Column("REQUESTER_ID")]
        public int requesterId { get; set; }

        [Column("REQUESTED_ID")]
        public int requestedId { get; set; }

        [Column("REQUEST_DATE_TIME")]
        public DateTime requestDateTime { get; set; }

        // FKs
        [ForeignKey("requesterId")]
        [InverseProperty("friendshipRequestsMade")]
        public virtual User requester { get; set; }

        [ForeignKey("requestedId")]
        [InverseProperty("frinedshipUsersRequested")]
        public virtual User requested { get; set; }
    }
}
