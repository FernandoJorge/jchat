﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace JChat.Utils
{
    static class Utils
    {
        static public void writeLog(String _class, String method, String lineNumber, String columnNumber, String message)
        {
            String dir = "Files/Logs";
            String logFile = Path.GetFullPath("Files/Logs/logs.txt");

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            if (!File.Exists(logFile))
                File.Create(logFile);

            StreamWriter strWriter = new StreamWriter(logFile,true);

            strWriter.WriteLine("<<<< LOG REPORT START >>>>");
            strWriter.WriteLine();
            strWriter.WriteLine("[ Date: "  + DateTime.UtcNow.ToString("yyyy-MM-dd") + " ]");
            strWriter.WriteLine("[ Time: " + DateTime.UtcNow.ToString("HH:mm:ss") + " ]");
            strWriter.WriteLine("[ Class: " + _class + " ]");
            strWriter.WriteLine("[ Method: " + method + " ]");
            strWriter.WriteLine("[ Line;Column: " + lineNumber + ";" + columnNumber + " ]");
            strWriter.WriteLine("[ Error message: " + message + " ]");
            strWriter.WriteLine();
            strWriter.WriteLine("<<<< LOG REPORT END >>>>");
            strWriter.WriteLine();
            strWriter.WriteLine();
            strWriter.Close();

        }
    }
}
