﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JChat
{
    /// <summary>
    /// Window form to display all users registerd in database
    /// </summary>
    public partial class ManageUsersWindow : Form
    {
        Form parent;
        Models.ServerApp serverApp = Models.ServerApp.Instance();
        DataTable allUsers = null;

        /// <summary>
        /// Construct
        /// </summary>
        /// <param name="parent">Parent´s window form object</param>
        public ManageUsersWindow(Form parent)
        {
            parent.Hide();
            this.parent = parent;
            InitializeComponent();

            allUsers = this.serverApp.getAllUsers();

            if (allUsers == null)
            {
                MessageBox.Show(this, "occurred an error while geting users information...", "Error...");
                this.parent.Show();
                this.Dispose();
                return;
            }

            dataGridView1.DataSource = allUsers;
            label2.Text = Convert.ToString(allUsers.Rows.Count);

            DataGridViewButtonColumn btUserManagement = new DataGridViewButtonColumn();
            btUserManagement.Text = "Consult...";
            btUserManagement.HeaderText = "USER´S INFORMATION";
            btUserManagement.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(btUserManagement);

            this.Show();
        }

        /// <summary>
        /// Window form Closed´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManageUsers_FormClosed(object sender, FormClosedEventArgs e)
        {
            parent.Show();
            this.Dispose();
        }

        /// <summary>
        /// Cell content click´s event handler to display a single user profile´s information window form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var gridView = (DataGridView)sender;

            if (dataGridView1.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                int userId = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[1].FormattedValue);
                UserProfileWindow userProfile = new UserProfileWindow(this, userId);
                userProfile.Show();
            }
        }

        /// <summary>
        /// Button click´s event handler to close current window form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.parent.Show();
            this.Dispose();
        }
    }
}
