namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableSESSIONS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GENDERS",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DESCRIPTION = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SESSIONS",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        LOGIN_DATE_TIME = c.DateTime(nullable: false),
                        LOGOUT_DATE_TIME = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.USERS", t => t.USER_ID)
                .Index(t => t.USER_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SESSIONS", "USER_ID", "dbo.USERS");
            DropIndex("dbo.SESSIONS", new[] { "USER_ID" });
            DropTable("dbo.SESSIONS");
            DropTable("dbo.GENDERS");
        }
    }
}
