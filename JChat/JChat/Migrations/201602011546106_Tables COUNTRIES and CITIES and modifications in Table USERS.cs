namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TablesCOUNTRIESandCITIESandmodificationsinTableUSERS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CITIES",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NAME = c.String(),
                        COUNTRY_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.COUNTRIES",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NAME = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.COUNTRIES");
            DropTable("dbo.CITIES");
        }
    }
}
