namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableUSERSENABLEDAttributenew : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.USERS", "ENABLED", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.USERS", "ENABLED");
        }
    }
}
