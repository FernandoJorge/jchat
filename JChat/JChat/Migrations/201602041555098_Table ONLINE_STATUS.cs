namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableONLINE_STATUS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ONLINE_STATUS",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DESCRIPTION = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.USERS", "ONLINE_STATUS_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.USERS", "ONLINE_STATUS_ID");
            AddForeignKey("dbo.USERS", "ONLINE_STATUS_ID", "dbo.ONLINE_STATUS", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.USERS", "ONLINE_STATUS_ID", "dbo.ONLINE_STATUS");
            DropIndex("dbo.USERS", new[] { "ONLINE_STATUS_ID" });
            DropColumn("dbo.USERS", "ONLINE_STATUS_ID");
            DropTable("dbo.ONLINE_STATUS");
        }
    }
}
