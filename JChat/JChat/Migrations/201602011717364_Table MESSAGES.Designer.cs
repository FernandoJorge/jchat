// <auto-generated />
namespace JChat.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class TableMESSAGES : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(TableMESSAGES));
        
        string IMigrationMetadata.Id
        {
            get { return "201602011717364_Table MESSAGES"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
