namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModificationsinTableCITIES : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.CITIES", "COUNTRY_ID");
            AddForeignKey("dbo.CITIES", "COUNTRY_ID", "dbo.COUNTRIES", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CITIES", "COUNTRY_ID", "dbo.COUNTRIES");
            DropIndex("dbo.CITIES", new[] { "COUNTRY_ID" });
        }
    }
}
