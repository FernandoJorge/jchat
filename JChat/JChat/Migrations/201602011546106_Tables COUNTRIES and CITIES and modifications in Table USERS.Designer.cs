// <auto-generated />
namespace JChat.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class TablesCOUNTRIESandCITIESandmodificationsinTableUSERS : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(TablesCOUNTRIESandCITIESandmodificationsinTableUSERS));
        
        string IMigrationMetadata.Id
        {
            get { return "201602011546106_Tables COUNTRIES and CITIES and modifications in Table USERS"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
