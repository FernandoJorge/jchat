namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableUSERSchanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.USERS", "FIRST_NAME", c => c.String(maxLength: 100));
            AddColumn("dbo.USERS", "LAST_NAME", c => c.String(maxLength: 100));
            DropColumn("dbo.USERS", "USERNAME");
        }
        
        public override void Down()
        {
            AddColumn("dbo.USERS", "USERNAME", c => c.String(maxLength: 100));
            DropColumn("dbo.USERS", "LAST_NAME");
            DropColumn("dbo.USERS", "FIRST_NAME");
        }
    }
}
