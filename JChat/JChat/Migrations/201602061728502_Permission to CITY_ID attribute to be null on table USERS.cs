namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PermissiontoCITY_IDattributetobenullontableUSERS : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.USERS", "CITY_ID", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.USERS", "CITY_ID", c => c.Int(nullable: false));
        }
    }
}
