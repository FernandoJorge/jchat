namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableCONTACTS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CONTACTS",
                c => new
                    {
                        USER_ID = c.Int(nullable: false),
                        CONTACT_ID = c.Int(nullable: false),
                        CONNECTION_DATE_TIME = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.USER_ID, t.CONTACT_ID })
                .ForeignKey("dbo.USERS", t => t.CONTACT_ID)
                .ForeignKey("dbo.USERS", t => t.USER_ID)
                .Index(t => t.USER_ID)
                .Index(t => t.CONTACT_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CONTACTS", "USER_ID", "dbo.USERS");
            DropForeignKey("dbo.CONTACTS", "CONTACT_ID", "dbo.USERS");
            DropIndex("dbo.CONTACTS", new[] { "CONTACT_ID" });
            DropIndex("dbo.CONTACTS", new[] { "USER_ID" });
            DropTable("dbo.CONTACTS");
        }
    }
}
