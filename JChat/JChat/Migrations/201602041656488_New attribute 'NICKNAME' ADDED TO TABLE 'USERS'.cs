namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewattributeNICKNAMEADDEDTOTABLEUSERS : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.USERS", "NICKNAME", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.USERS", "NICKNAME");
        }
    }
}
