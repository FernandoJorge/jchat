namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewattributeREGISTER_DATEintableUSERS : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.USERS", "REGISTER_DATE", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.USERS", "REGISTER_DATE");
        }
    }
}
