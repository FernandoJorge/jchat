namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewtableFRIENDSHIP_REQUESTS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FRIENDSHIP_REQUESTS",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        REQUESTER_ID = c.Int(nullable: false),
                        REQUESTED_ID = c.Int(nullable: false),
                        REQUEST_DATE_TIME = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.USERS", t => t.REQUESTED_ID)
                .ForeignKey("dbo.USERS", t => t.REQUESTER_ID)
                .Index(t => t.REQUESTER_ID)
                .Index(t => t.REQUESTED_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FRIENDSHIP_REQUESTS", "REQUESTER_ID", "dbo.USERS");
            DropForeignKey("dbo.FRIENDSHIP_REQUESTS", "REQUESTED_ID", "dbo.USERS");
            DropIndex("dbo.FRIENDSHIP_REQUESTS", new[] { "REQUESTED_ID" });
            DropIndex("dbo.FRIENDSHIP_REQUESTS", new[] { "REQUESTER_ID" });
            DropTable("dbo.FRIENDSHIP_REQUESTS");
        }
    }
}
