namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableMESSAGES : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MESSAGES",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SENDER_ID = c.Int(nullable: false),
                        RECEIVER_ID = c.Int(nullable: false),
                        DATE_TIME = c.DateTime(nullable: false),
                        MESSAGE = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.USERS", t => t.RECEIVER_ID)
                .ForeignKey("dbo.USERS", t => t.SENDER_ID)
                .Index(t => t.SENDER_ID)
                .Index(t => t.RECEIVER_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MESSAGES", "SENDER_ID", "dbo.USERS");
            DropForeignKey("dbo.MESSAGES", "RECEIVER_ID", "dbo.USERS");
            DropIndex("dbo.MESSAGES", new[] { "RECEIVER_ID" });
            DropIndex("dbo.MESSAGES", new[] { "SENDER_ID" });
            DropTable("dbo.MESSAGES");
        }
    }
}
