namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableUSERSfullnameattributedeletion : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.USERS", "FULLNAME");
        }
        
        public override void Down()
        {
            AddColumn("dbo.USERS", "FULLNAME", c => c.String(maxLength: 255));
        }
    }
}
