namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewattributeBLOCKEDADDEDTOTABLECONTACT : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CONTACTS", "BLOCKED", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CONTACTS", "BLOCKED");
        }
    }
}
