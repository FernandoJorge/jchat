namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableUSERS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.USERS",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        USERNAME = c.String(maxLength: 100),
                        PASSWORD = c.String(maxLength: 20),
                        FULLNAME = c.String(maxLength: 255),
                        EMAIL = c.String(),
                        CITY_ID = c.Int(nullable: false),
                        GENDER_ID = c.Int(nullable: false),
                        BIRTH_DATE = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.USERS");
        }
    }
}
