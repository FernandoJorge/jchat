namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PermitnullvalueofLOGOUT_DATE_TIMEattributeatSESSIONStable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SESSIONS", "LOGOUT_DATE_TIME", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SESSIONS", "LOGOUT_DATE_TIME", c => c.DateTime(nullable: false));
        }
    }
}
