namespace JChat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewattributeLOGGEDinUSERStable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.USERS", "LOGGED", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.USERS", "LOGGED");
        }
    }
}
