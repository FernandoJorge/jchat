﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JChat
{
    /// <summary>
    /// Window form to display user profile´s information
    /// </summary>
    public partial class UserProfileWindow : Form
    {
        Form parent;
        int userId;
        Models.ServerApp serverApp = Models.ServerApp.Instance();
        Data.User user = null;

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="parent">Parent window form object</param>
        /// <param name="userId">User´s identification number</param>
        public UserProfileWindow(Form parent, int userId)
        {
            this.parent = parent;
            this.userId = userId;
            parent.Hide();

            this.user = serverApp.getUser(userId);
            List<Data.City> allCities = serverApp.getCities();
            List<Data.Country> allCountries = serverApp.getCountries();
            List<Data.Gender> allGenders = serverApp.getGenders();

            if (user == null || allCities == null || allCountries == null || allGenders == null)
            {
                MessageBox.Show(parent, "occurred an error. It´s not possible to show user´s profile...", "Error...");
                parent.Show();
                this.Dispose();
            }

            InitializeComponent();

            this.Text = "JChat - " + user.firstName + " " + user.lastName + "´s profile...";

            String gender = allGenders.Where(g => g.id == user.genderId).FirstOrDefault().description;
            String city = (user.cityId != -1 ? allCities.Where(ct => ct.id == user.cityId).FirstOrDefault().name : "No city...");
            String country = (user.cityId != -1 ? allCountries.Where(ctr => ctr.id == allCities.Where(ct => ct.id == user.cityId).FirstOrDefault().countryId).FirstOrDefault().name : "No country...");

            label11.Text = Convert.ToString(user.id);
            label12.Text = user.firstName;
            label13.Text = user.lastName;
            label14.Text = user.nickname;
            label15.Text = country;
            label16.Text = city;
            label17.Text = gender;
            label18.Text = user.email;
            label19.Text = user.registerDate.ToString("dd-MM-yyyy");
            label20.Text = (user.enabled ? "Yes" : "No");

            button2.Text = (user.enabled ? "Disable" : "Enable");
        }

        /// <summary>
        /// Window form closed´s event handler to close current window form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserProfileWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.parent.Show();
            this.Dispose();
        }

        /// <summary>
        /// Button click´s event handler to close current window form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.parent.Show();
            this.Dispose();
        }

        /// <summary>
        /// Button click´s event handler to enable/disable a user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Boolean result = this.serverApp.setEnableUser(this.userId);

            if (!result)
            {
                MessageBox.Show(this, "occurred an error while tryng to change enabled state...", "Error...");
                return;
            }

            this.user = this.serverApp.getUser(this.userId);

            if (this.user == null)
            {
                MessageBox.Show(this, "occurred an error while updating user profile...", "Error...");
                button2.Enabled = false;
                return;
            }

            label20.Text = (user.enabled ? "Yes" : "No");
            button2.Text = (user.enabled ? "Disable" : "Enable");

            DataGridView gridView = (DataGridView)this.parent.Controls.Find("dataGridView1", true)[0];
            Label label = (Label)this.parent.Controls.Find("label2", true)[0];

            DataTable allUsers = this.serverApp.getAllUsers();

            if (allUsers == null)
            {
                MessageBox.Show(this, "occurred an error while geting users information...", "Error...");
                return;
            }

            gridView.DataSource = allUsers;
            label.Text = Convert.ToString(allUsers.Rows.Count);
        }

        /// <summary>
        /// Button click´s event handler to delete a user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {

            DialogResult dialResult = MessageBox.Show(this, "Confirm user deletion?", "Deletion confirmation...", MessageBoxButtons.YesNo);

            if (dialResult == DialogResult.Yes)
            {
                Boolean result = this.serverApp.deleteUser(this.userId);

                if (!result)
                {
                    MessageBox.Show(this, "occurred an error while tryng to delete the user...", "Error...");
                    return;
                }

                DataGridView gridView = (DataGridView)this.parent.Controls.Find("dataGridView1", true)[0];
                Label label = (Label)this.parent.Controls.Find("label2", true)[0];

                DataTable allUsers = this.serverApp.getAllUsers();

                if (allUsers == null)
                {
                    MessageBox.Show(this, "occurred an error while geting users information...", "Error...");
                    this.parent.Show();
                    this.Dispose();
                }

                gridView.DataSource = allUsers;
                label.Text = Convert.ToString(allUsers.Rows.Count);
            }
        }

        /// <summary>
        /// Button click´s event handler to display user´s statistics
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            UserStatisticsWindow stats = new UserStatisticsWindow(this, this.userId);
            stats.Show();
        }
    }
}
