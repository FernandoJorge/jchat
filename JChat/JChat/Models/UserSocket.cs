﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace JChat.Models
{
    /// <summary>
    /// class that represents a socket information for a client
    /// </summary>
    public class UserSocket
    {
        public int userId { get; set; }

        public Socket clientSocket { get; set; }
    }
}
