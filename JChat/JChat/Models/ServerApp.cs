﻿using JChat.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace JChat.Models
{
    /// <summary>
    /// Class that represents the chat server application´s model
    /// </summary>
    public class ServerApp
    {
        private static ServerApp _instance;
        public List<JChat.Data.Session> usersSessions = null;
        public List<Models.UserSocket> userSockets = null;

        protected ServerApp()
        {

        }

        // Singleton Pattern design
        public static ServerApp Instance()
        {
            if (_instance == null)
                _instance = new ServerApp();

            return _instance;
        }

        /// <summary>
        /// Creates a new session for a user
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>False if occures an error; True if success</returns>
        public Boolean createNewUserSession(int userId)
        {
            if (this.usersSessions == null)
                this.usersSessions = new List<JChat.Data.Session>();

            JChat.Data.Session userSession = new JChat.Data.Session();

            userSession.userId = userId;
            userSession.loginDateTime = DateTime.Now;

            JChat.Data.JChatDB db = new JChat.Data.JChatDB();

            try
            {
                db.Sessions.Add(userSession);
                db.SaveChanges();

            }
            catch (Exception ex)
            {

                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return false;
            }

            this.usersSessions.Add(userSession);

            return true;
        }

        /// <summary>
        /// Adds new client socket to "userSockets" variable
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <param name="client">Client´s socket object</param>
        public void addNewSocketClient(int userId, Socket client)
        {
            if (this.userSockets == null)
                this.userSockets = new List<UserSocket>();

            this.userSockets.Add(new UserSocket()
            {
                userId = userId,
                clientSocket = client
            });
        }

        /// <summary>
        /// Logout of user
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>False if occurred an error; True if success</returns>
        public Boolean logoutUser(int userId)
        {
            JChat.Data.JChatDB db = new JChat.Data.JChatDB();

            IEnumerable<User> allUsers = from users in db.Users
                                         where users.id == userId
                                         select users;
            try
            {
                User user = allUsers.FirstOrDefault();
                user.logged = false;

                db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            catch (NullReferenceException ex)
            {

                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return false;
            }

            JChat.Data.Session userSession = this.usersSessions.Where(us => us.userId == userId).FirstOrDefault();
            Models.UserSocket userSocket = this.userSockets.Where(us => us.userId == userId).FirstOrDefault();

            userSession.logoutDateTime = DateTime.Now;

            try
            {
                db.Entry(userSession).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return false;
            }

            this.usersSessions.Remove(userSession);
            this.userSockets.Remove(userSocket);

            if (this.usersSessions.Count() == 0)
            {
                this.usersSessions = null;
                GC.Collect();
            }

            if (this.userSockets.Count() == 0)
            {
                this.userSockets = null;
                GC.Collect();
            }

            return true;
        }

        /// <summary>
        /// Saves a message from user
        /// </summary>
        /// <param name="senderId">Identification number of the user sending the message</param>
        /// <param name="receiverId">Identification number of the user receiving the message</param>
        /// <param name="message">User´s message</param>
        /// <param name="dateTime">Date of the message</param>
        public void saveNewUserMessage(String senderId, String receiverId, String message, String dateTime)
        {
            int sender = Convert.ToInt32(senderId);
            int receiver = Convert.ToInt32(receiverId);
            DateTime dt = Convert.ToDateTime(dateTime);

            // Saves new message
            JChatDB db = new JChatDB();

            Data.Message newMessage = new Data.Message()
            {
                senderId = sender,
                receiverId = receiver,
                messsage = message,
                dateTime = dt
            };

            try
            {
                db.Messages.Add(newMessage);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
            }
        }

        /// <summary>
        /// Handles the clients´ requests
        /// </summary>
        /// <param name="content">XML message sent by the client request</param>
        /// <param name="server">Server socket object</param>
        /// <param name="client">Client socket object</param>
        public void sendResponseToClient(String content, JChat.Data.ServerSockets.AsynchronousSocketListener server, Socket client)
        {
            XDocument xmlDocument = XDocument.Parse(content);

            String messageType = xmlDocument.Element("frame").Element("header").Element("message_type").Value.Trim();

            if (messageType.Equals("ALL_CITIES"))
            {
                XDocument xmlCities = this.getAllCities();

                server.Send(client, xmlCities.ToString());
            }

            if (messageType.Equals("ALL_COUNTRIES"))
            {
                XDocument xmlCountries = this.getAllCountries();

                server.Send(client, xmlCountries.ToString());
            }

            if (messageType.Equals("ALL_CITIES_OF_COUNTRY"))
            {
                String cityId = xmlDocument.Element("frame").Element("body").Element("arguments").Element("country_id").Value.Trim();

                XDocument xmlCities = this.getAllCitiesByCountryId(cityId);

                server.Send(client, xmlCities.ToString());
            }

            if (messageType.Equals("ALL_GENDERS"))
            {
                XDocument xmlGenders = this.getAllGenders();

                server.Send(client, xmlGenders.ToString());
            }

            if (messageType.Equals("USER_CONTACTS"))
            {
                String userId = xmlDocument.Element("frame").Element("body").Element("arguments").Element("user_id").Value.Trim();

                XDocument xmlContacts = this.getAllContacts(userId);

                server.Send(client, xmlContacts.ToString());
            }

            if (messageType.Equals("ALL_STATUS"))
            {
                XDocument xmlStatus = this.getAllOnlineStatus();

                server.Send(client, xmlStatus.ToString());
            }

            if (messageType.Equals("ALL_MESSAGES_BETWEEN_USERS"))
            {
                XElement userOne = xmlDocument.Element("frame").Element("body").Element("arguments").Element("user_one");

                XDocument xmlMessages = this.getAllMessages(userOne.Value.Trim());

                server.Send(client, xmlMessages.ToString());
            }

            if (messageType.Equals("EXISTS_EMAIL"))
            {
                XElement email = xmlDocument.Element("frame").Element("body").Element("arguments").Element("email");

                XDocument xmlExist = this.getIfExistEmail(email.Value.Trim());

                server.Send(client, xmlExist.ToString());
            }

            if (messageType.Equals("NEW_USER_ACCOUNT"))
            {
                XDocument xmlAccount = this.saveNewUserAccount(content);

                server.Send(client, xmlAccount.ToString());
            }

            if (messageType.Equals("EDIT_USER_PROFILE"))
            {
                XDocument xmlProfile = this.editUserProfile(content);

                server.Send(client, xmlProfile.ToString());
            }

            if (messageType.Equals("USER_PROFILE"))
            {
                XElement email = xmlDocument.Element("frame").Element("body").Element("arguments").Element("email");

                XDocument xmlUserProfile = this.getUserProfile(email.Value.Trim());

                server.Send(client, xmlUserProfile.ToString());
            }

            if (messageType.Equals("USER_AUTHENTICATION"))
            {
                XElement email = xmlDocument.Element("frame").Element("body").Element("arguments").Element("email");
                XElement password = xmlDocument.Element("frame").Element("body").Element("arguments").Element("password");

                XDocument xmlAuthent = this.authenticateUser(email.Value.Trim(), password.Value.Trim(), client);

                server.Send(client, xmlAuthent.ToString());
            }

            if (messageType.Equals("USER_LOGOUT"))
            {
                XElement userId = xmlDocument.Element("frame").Element("body").Element("arguments").Element("user_id");

                int usrId = Convert.ToInt32(userId.Value.Trim());

                Models.ServerApp serverApp = Models.ServerApp.Instance();
                this.logoutUser(usrId);

                server.Send(client, "ok");
            }

            if (messageType.Equals("USER_MESSAGE"))
            {
                XElement senderId = xmlDocument.Element("frame").Element("header").Element("sender");
                XElement receiverId = xmlDocument.Element("frame").Element("header").Element("receiver");
                XElement message = xmlDocument.Element("frame").Element("body").Element("communication").Element("message");
                XElement dateTime = xmlDocument.Element("frame").Element("body").Element("communication").Element("date_time");

                this.saveNewUserMessage(senderId.Value.Trim(), receiverId.Value.Trim(), message.Value.Trim(), dateTime.Value.Trim());
            }

            if (messageType.Equals("USER_SEARCH"))
            {
                XElement userSearch = xmlDocument.Element("frame").Element("body").Element("arguments").Element("search");

                XDocument xmlUserSearch = this.getUserSearch(userSearch.Value.Trim());

                server.Send(client, xmlUserSearch.ToString());
            }

            if (messageType.Equals("FRIENDSHIP_REQUEST"))
            {
                XElement requester = xmlDocument.Element("frame").Element("header").Element("sender");
                XElement requested = xmlDocument.Element("frame").Element("header").Element("receiver");

                int requesterId = Convert.ToInt32(requester.Value.Trim());
                int requestedId = Convert.ToInt32(requested.Value.Trim());

                JChatDB db = new JChatDB();

                IEnumerable<FriendshipRequest> allRequests = from requests in db.FriendshipRequests
                                                             where requests.requesterId == requesterId && requests.requestedId == requestedId
                                                             select requests;

                if (allRequests.Count() == 0)
                {
                    FriendshipRequest request = new FriendshipRequest()
                    {
                        requestDateTime = DateTime.Now,
                        requesterId = requesterId,
                        requestedId = requestedId
                    };

                    try
                    {
                        db.FriendshipRequests.Add(request);
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine("Error: " + ex.Message);
                        Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                    }
                }
            }

            if (messageType.Equals("FRIENDSHIP_REQUEST_ACCEPTANCE"))
            {
                XElement requester = xmlDocument.Element("frame").Element("header").Element("sender");
                XElement requested = xmlDocument.Element("frame").Element("header").Element("receiver");

                int requesterId = Convert.ToInt32(requester.Value.Trim());
                int requestedId = Convert.ToInt32(requested.Value.Trim());

                JChatDB db = new JChatDB();

                IEnumerable<Contact> allContacts = from contacts in db.Contacts
                                                   where (contacts.userId == requesterId && contacts.contactId == requestedId) ||
                                                         (contacts.userId == requestedId && contacts.contactId == requesterId)
                                                   select contacts;

                IEnumerable<FriendshipRequest> allRequests = from requests in db.FriendshipRequests
                                                             where requests.requesterId == requesterId && requests.requestedId == requestedId
                                                             select requests;

                FriendshipRequest request = null;

                try
                {
                    request = allRequests.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {

                    Console.WriteLine("Error: " + ex.Message);
                    Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                    return;
                }

                // If already exists a the friendship do nothing
                if (allContacts.Count() > 0)
                {
                    try
                    {
                        db.FriendshipRequests.Remove(request);

                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                        Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                        return;
                    }

                    return;
                }

                // Add new contact to both ends
                Contact contactRequester = new Contact()
                {
                    blocked = false,
                    connetionDateTime = DateTime.Now,
                    contactId = requestedId,
                    userId = requesterId
                };

                Contact contactRequested = new Contact()
                {
                    blocked = false,
                    connetionDateTime = DateTime.Now,
                    contactId = requesterId,
                    userId = requestedId
                };

                try
                {
                    db.Contacts.Add(contactRequester);
                    db.Contacts.Add(contactRequested);
                    db.FriendshipRequests.Remove(request);

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                    Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                    return;
                }
            }

            if (messageType.Equals("USER_FRIENDSHIP_REQUESTS"))
            {
                XElement userId = xmlDocument.Element("frame").Element("body").Element("arguments").Element("user_id");
                XDocument xmlUsers = this.getUserFriendshipRequests(userId.Value.Trim());

                server.Send(client, xmlUsers.ToString());
            }

            if (messageType.Equals("USER_STATUS_CHANGE"))
            {
                XElement userId = xmlDocument.Element("frame").Element("header").Element("sender");
                XElement statusId = xmlDocument.Element("frame").Element("body").Element("arguments").Element("status_id");

                int usrId = Convert.ToInt32(userId.Value.Trim());
                int stsId = Convert.ToInt32(statusId.Value.Trim());

                JChatDB db = new JChatDB();

                IEnumerable<User> allUsers = from users in db.Users
                                             where users.id == usrId
                                             select users;

                User user = null;

                try
                {
                    user = allUsers.FirstOrDefault();
                }
                catch (NullReferenceException ex)
                {

                    Console.WriteLine("Error: " + ex.Message);
                    Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                    return;
                }

                user.onlineStatusId = stsId;

                try
                {
                    db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Error: " + ex.Message);
                    Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                    return;
                }
            }

            if (messageType.Equals("DELETE_CONTACT"))
            {
                XElement userId = xmlDocument.Element("frame").Element("header").Element("sender");
                XElement contactId = xmlDocument.Element("frame").Element("header").Element("receiver");

                int usrId = Convert.ToInt32(userId.Value.Trim());
                int ctdId = Convert.ToInt32(contactId.Value.Trim());

                JChatDB db = new JChatDB();

                IEnumerable<Data.Message> allMessages = from messages in db.Messages
                                                        where (messages.senderId == usrId && messages.receiverId == ctdId) ||
                                                              (messages.receiverId == usrId && messages.senderId == ctdId)
                                                        select messages;

                IEnumerable<Contact> allContacts = from contacts in db.Contacts
                                                   where (contacts.userId == usrId && contacts.contactId == ctdId) ||
                                                         (contacts.userId == ctdId && contacts.contactId == usrId)
                                                   select contacts;

                // Remove all messages exchanged between users
                if (allMessages.Count() > 0)
                    foreach (Data.Message message in allMessages)
                        db.Messages.Remove(message);

                // Remove friendship connections
                if (allContacts.Count() > 0)
                    foreach (Contact contact in allContacts)
                        db.Contacts.Remove(contact);
                else return;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Error: " + ex.Message);
                    Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                    return;
                }
            }
        }

        /// <summary>
        /// Authenticates user
        /// </summary>
        /// <param name="email">User´s email address</param>
        /// <param name="password">User´s password</param>
        /// <param name="client">Client´s socket</param>
        /// <returns>XML response: "nok" if wrong credentials; "ok" if valid credentials; "error" if it happens an error</returns>
        private XDocument authenticateUser(String email, String password, Socket client)
        {
            JChatDB db = new JChatDB();

            IEnumerable<User> allUsers = from users in db.Users
                                         where users.email == email && users.password == password
                                         select users;

            XDocument xmlDoc = null;
            XElement xmlProfile = null;
            User profile = null;

            try
            {

                profile = allUsers.FirstOrDefault();

                if (profile == null)
                {
                    xmlProfile = new XElement("authentication", "nok");
                    xmlDoc = this.getXmlDocument("USER_AUTHENTICATION_RESPONSE", "", "", xmlProfile);

                    return xmlDoc;
                }

                // Update user´s session
                ServerApp serverApp = ServerApp.Instance();
                serverApp.createNewUserSession(profile.id);

                // Update list of sockets clients
                serverApp.addNewSocketClient(profile.id, client);

                Task.Factory.StartNew(() =>
                {

                    profile.logged = true;

                    try
                    {
                        db.Entry(profile).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine("Error: " + ex.Message);
                        Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                        return;
                    }
                });

                xmlProfile = new XElement("authentication", "ok");
                xmlDoc = this.getXmlDocument("USER_AUTHENTICATION_RESPONSE", "", "", xmlProfile);

                return xmlDoc;

            }
            catch (NullReferenceException ex)
            {

                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);

                xmlProfile = new XElement("authentication", "error");
                xmlDoc = this.getXmlDocument("USER_AUTHENTICATION_RESPONSE", "", "", xmlProfile);

                return xmlDoc;
            }
        }

        /// <summary>
        /// XML message template for all the XML messages
        /// </summary>
        /// <param name="messageType">XML type of message</param>
        /// <param name="sender">Identification number of the user sending the message</param>
        /// <param name="receiver">Identification number of the user receiving the message</param>
        /// <param name="body">Message to send</param>
        /// <returns>XML message reponse of document</returns>
        private XDocument getXmlDocument(string messageType, String sender, String receiver, XElement body)
        {
            XDocument xmlDoc = new XDocument(new XDeclaration("2.0", "UTF-8", "yes"));
            XElement frame = new XElement("frame");

            XElement header = new XElement("header");
            header.Add(new XElement("message_type", messageType));
            header.Add(new XElement("sender", sender));
            header.Add(new XElement("receiver", receiver));

            XElement bodyMessage = new XElement("body");
            bodyMessage.Add(body);

            frame.Add(header);
            frame.Add(bodyMessage);

            xmlDoc.Add(frame);

            return xmlDoc;
        }

        /// <summary>
        /// Retrieve all the cities´ information
        /// </summary>
        /// <returns>XML message response: "null" if there are no cities; All cities´information</returns>
        private XDocument getAllCities()
        {

            JChatDB db = new JChatDB();

            IEnumerable<City> allCities = from cities in db.Cities
                                          orderby cities.name
                                          select cities;

            try
            {

                XDocument xmlDoc = null;
                XElement xmlCities = new XElement("cities");

                if (allCities.Count() == 0)
                {
                    xmlCities.Add(new XElement("city", "null"));
                    xmlDoc = this.getXmlDocument("ALL_CITIES", "", "", xmlCities);

                    return xmlDoc;
                }

                foreach (City city in allCities)
                {

                    XElement xmlCity = new XElement("city");
                    xmlCity.Add(new XElement("id", Convert.ToString(city.id)));
                    xmlCity.Add(new XElement("name", city.name));
                    xmlCity.Add(new XElement("country_id", Convert.ToString(city.countryId)));
                    xmlCities.Add(xmlCity);
                }

                xmlDoc = this.getXmlDocument("ALL_CITIES", "", "", xmlCities);
                return xmlDoc;
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Retrieve all the countries´ information
        /// </summary>
        /// <returns>XML message response: "null" if there are no countries; All countries´ information</returns>
        private XDocument getAllCountries()
        {
            JChatDB db = new JChatDB();

            IEnumerable<Country> allCountries = from countries in db.Countries
                                                orderby countries.name
                                                select countries;

            try
            {
                XDocument xmlDoc = null;
                XElement xmlCountries = new XElement("countries");

                if (allCountries.Count() == 0)
                {
                    xmlCountries.Add(new XElement("country", "null"));
                    xmlDoc = this.getXmlDocument("ALL_COUNTRIES", "", "", xmlCountries);

                    return xmlDoc;
                }

                foreach (Country country in allCountries)
                {

                    XElement xmlCountry = new XElement("country");
                    xmlCountry.Add(new XElement("id", Convert.ToString(country.id)));
                    xmlCountry.Add(new XElement("name", country.name));
                    xmlCountries.Add(xmlCountry);
                }

                xmlDoc = this.getXmlDocument("ALL_COUNTRIES", "", "", xmlCountries);
                return xmlDoc;
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Retrieve all the cities´s information of a given country
        /// </summary>
        /// <param name="countryId">City´s identification number</param>
        /// <returns>XML message response: "null" if there are no cities; All cities´ information</returns>
        private XDocument getAllCitiesByCountryId(String countryId)
        {
            int ctrId = Convert.ToInt32(countryId);

            JChatDB db = new JChatDB();

            IEnumerable<City> allCities = from cities in db.Cities
                                          where cities.countryId == ctrId
                                          orderby cities.name
                                          select cities;

            try
            {
                XDocument xmlDoc = null;
                XElement xmlCities = new XElement("cities");

                if (allCities.Count() == 0)
                {
                    xmlCities.Add(new XElement("city", "null"));
                    xmlDoc = this.getXmlDocument("ALL_CITIES_OF_COUNTRY", "", "", xmlCities);

                    return xmlDoc;
                }

                foreach (City city in allCities)
                {

                    XElement xmlCity = new XElement("city");
                    xmlCity.Add(new XElement("id", Convert.ToString(city.id)));
                    xmlCity.Add(new XElement("name", city.name));
                    xmlCities.Add(xmlCity);
                }

                xmlDoc = this.getXmlDocument("ALL_CITIES_OF_COUNTRY", "", "", xmlCities);
                return xmlDoc;
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Retrieve all the genders´ information
        /// </summary>
        /// <returns>XML message response: "null" if there are no genders; All genders´ infromation</returns>
        private XDocument getAllGenders()
        {
            JChatDB db = new JChatDB();

            IEnumerable<Gender> allGenders = from genders in db.Genders
                                             orderby genders.description
                                             select genders;

            try
            {
                XDocument xmlDoc = null;
                XElement xmlGenders = new XElement("genders");

                if (allGenders.Count() == 0)
                {
                    xmlGenders.Add(new XElement("gender", "null"));
                    xmlDoc = this.getXmlDocument("ALL_GENDERS", "", "", xmlGenders);

                    return xmlDoc;
                }


                foreach (Gender gender in allGenders)
                {

                    XElement xmlGender = new XElement("gender");
                    xmlGender.Add(new XElement("id", Convert.ToString(gender.id)));
                    xmlGender.Add(new XElement("name", gender.description));
                    xmlGenders.Add(xmlGender);
                }

                xmlDoc = this.getXmlDocument("ALL_GENDERS", "", "", xmlGenders);
                return xmlDoc;
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Retrieve all user contacts´ information
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>XML message response: "null" if there are no contacts; All user´s contacts</returns>
        private XDocument getAllContacts(String userId)
        {
            int usrId = Convert.ToInt32(userId);

            JChatDB db = new JChatDB();

            var allUsers = from contacts in db.Contacts
                           join users in db.Users on contacts.contactId equals users.id
                           where contacts.userId == usrId
                           select new
                           {

                               id = users.id,
                               email = users.email,
                               nickname = users.nickname,
                               firstname = users.firstName,
                               lastname = users.lastName,
                               blocked = contacts.blocked,
                               birthdate = users.birthDate,
                               genderId = users.genderId,
                               enabled = users.enabled,
                               cityId = users.cityId,
                               statusId = users.onlineStatusId,
                               logged = users.logged,
                               connectionDate = contacts.connetionDateTime
                           };

            try
            {
                XDocument xmlDoc = null;
                XElement xmlContacts = new XElement("contacts");

                if (allUsers.Count() == 0)
                {
                    xmlContacts.Add(new XElement("contact", "null"));
                    xmlDoc = this.getXmlDocument("USER_CONTACTS", "", "", xmlContacts);

                    return xmlDoc;
                }



                foreach (var contact in allUsers)
                {

                    XElement xmlContact = new XElement("contact");
                    xmlContact.Add(new XElement("id", Convert.ToString(contact.id)));
                    xmlContact.Add(new XElement("email", contact.email));
                    xmlContact.Add(new XElement("nickname", contact.nickname));
                    xmlContact.Add(new XElement("firstname", contact.firstname));
                    xmlContact.Add(new XElement("lastname", contact.lastname));
                    xmlContact.Add(new XElement("blocked", (contact.blocked == true ? "yes" : "no")));
                    xmlContact.Add(new XElement("birthdate", contact.birthdate.ToString()));
                    xmlContact.Add(new XElement("gender_id", Convert.ToString(contact.genderId)));
                    xmlContact.Add(new XElement("enabled", (contact.enabled == true ? "yes" : "no")));
                    xmlContact.Add(new XElement("status_id", Convert.ToString(contact.statusId)));
                    xmlContact.Add(new XElement("connection_date", contact.connectionDate.ToString()));
                    xmlContact.Add(new XElement("logged", (contact.logged == true ? "yes" : "no")));
                    xmlContact.Add(new XElement("city_id", (contact.cityId == null ? "null" : Convert.ToString(contact.cityId))));
                    xmlContacts.Add(xmlContact);
                }

                xmlDoc = this.getXmlDocument("USER_CONTACTS", "", "", xmlContacts);
                return xmlDoc;
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }

        }

        /// <summary>
        /// Retrieve user´s information
        /// </summary>
        /// <param name="email">User´s email address</param>
        /// <returns>XML message response: "null" if there´s no profile; User profile´s information</returns>
        private XDocument getUserProfile(String email)
        {
            JChatDB db = new JChatDB();

            IEnumerable<User> allUsers = from users in db.Users
                                         where users.email == email
                                         select users;

            XDocument xmlDoc = null;
            XElement xmlProfile = null;

            try
            {
                User profile = allUsers.FirstOrDefault();



                if (profile == null)
                {
                    xmlProfile = new XElement("profile", "null");
                    xmlDoc = this.getXmlDocument("USER_PROFILE", "", "", xmlProfile);

                    return xmlDoc;
                }

                xmlProfile = new XElement("profile");
                xmlProfile.Add(new XElement("id", Convert.ToString(profile.id)));
                xmlProfile.Add(new XElement("email", profile.email));
                xmlProfile.Add(new XElement("nickname", profile.nickname));
                xmlProfile.Add(new XElement("firstname", profile.firstName));
                xmlProfile.Add(new XElement("lastname", profile.lastName));
                xmlProfile.Add(new XElement("birthdate", profile.birthDate.Date.ToString()));
                xmlProfile.Add(new XElement("gender_id", Convert.ToString(profile.genderId)));
                xmlProfile.Add(new XElement("enabled", (profile.enabled == true ? "yes" : "no")));
                xmlProfile.Add(new XElement("status_id", Convert.ToString(profile.onlineStatusId)));
                xmlProfile.Add(new XElement("logged", (profile.logged == true ? "yes" : "no")));
                xmlProfile.Add(new XElement("city_id", (profile.cityId == null ? "null" : Convert.ToString(profile.cityId))));

                xmlDoc = this.getXmlDocument("USER_PROFILE_RESPONSE", "", "", xmlProfile);
                return xmlDoc;
            }
            catch (NullReferenceException ex)
            {

                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);

                xmlProfile = new XElement("profile", "error");
                xmlDoc = this.getXmlDocument("USER_PROFILE", "", "", xmlProfile);

                return xmlDoc;
            }
        }

        /// <summary>
        /// Retrieve all the online status´ information
        /// </summary>
        /// <returns>XML message response: "null" if there are no genders; All genders´ information</returns>
        private XDocument getAllOnlineStatus()
        {
            JChatDB db = new JChatDB();

            IEnumerable<OnlineStatus> allStatus = from status in db.OnlineStatus
                                                  select status;

            try
            {
                XDocument xmlDoc = null;
                XElement xmlAllStatus = new XElement("all_status");

                if (allStatus.Count() == 0)
                {
                    xmlAllStatus.Add(new XElement("status", "null"));
                    xmlDoc = this.getXmlDocument("ALL_STATUS", "", "", xmlAllStatus);

                    return xmlDoc;
                }

                foreach (OnlineStatus status in allStatus)
                {

                    XElement xmlStatus = new XElement("status");
                    xmlStatus.Add(new XElement("id", Convert.ToString(status.id)));
                    xmlStatus.Add(new XElement("description", status.description));
                    xmlAllStatus.Add(xmlStatus);
                }

                xmlDoc = this.getXmlDocument("ALL_STATUS", "", "", xmlAllStatus);
                return xmlDoc;
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Retrieve all user conversation messages´ information
        /// </summary>
        /// <param name="userOne">User´s identification information</param>
        /// <returns>XML message response: "null" if there are no messages; All messages´ information</returns>
        private XDocument getAllMessages(String userOne)
        {
            int usrOne = Convert.ToInt32(userOne);

            JChatDB db = new JChatDB();

            IEnumerable<Data.Message> allMessages = from messages in db.Messages
                                                    where messages.senderId == usrOne || messages.receiverId == usrOne
                                                    orderby messages.dateTime
                                                    select messages;

            try
            {
                XDocument xmlDoc = null;
                XElement xmlMessages = new XElement("messages");

                if (allMessages.Count() == 0)
                {
                    xmlMessages.Add(new XElement("message", "null"));
                    xmlDoc = this.getXmlDocument("ALL_MESSAGES_BETWEEN_USERS", "", "", xmlMessages);

                    return xmlDoc;
                }

                foreach (Data.Message message in allMessages)
                {

                    XElement xmlMessage = new XElement("message");
                    xmlMessage.Add(new XElement("message_id", Convert.ToString(message.id)));
                    xmlMessage.Add(new XElement("sender_id", Convert.ToString(message.senderId)));
                    xmlMessage.Add(new XElement("receiver_id", Convert.ToString(message.receiverId)));
                    xmlMessage.Add(new XElement("message_text", message.messsage));
                    xmlMessage.Add(new XElement("datetime", message.dateTime.ToString()));
                    xmlMessages.Add(xmlMessage);
                }

                xmlDoc = this.getXmlDocument("ALL_MESSAGES_BETWEEN_USERS", "", "", xmlMessages);
                return xmlDoc;
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Validate new user account´s email address
        /// </summary>
        /// <param name="email">New user´s email address</param>
        /// <returns>XML message response: "exists" if email address already exists; "nexists" if email address does not exist</returns>
        private XDocument getIfExistEmail(String email)
        {
            JChatDB db = new JChatDB();

            IEnumerable<User> allUsers = from users in db.Users
                                         where users.email == email
                                         select users;

            XDocument xmlDoc = null;

            try
            {
                XElement xmlEmails = null;
                User user = allUsers.FirstOrDefault();

                if (user != null)
                {
                    xmlEmails = new XElement("email", "exists");
                    xmlDoc = this.getXmlDocument("EXISTS_EMAIL", "", "", xmlEmails);
                }
                else
                {
                    xmlEmails = new XElement("email", "nexists");
                    xmlDoc = this.getXmlDocument("EXISTS_EMAIL", "", "", xmlEmails);
                }

                return xmlDoc;
            }
            catch (NullReferenceException ex)
            {

                XElement xmlEmails = new XElement("email", "error");
                xmlDoc = this.getXmlDocument("EXISTS_EMAIL", "", "", xmlEmails);

                return xmlDoc;
            }
        }

        /// <summary>
        /// Saves new user´s account
        /// </summary>
        /// <param name="xmlUserContent">XML content about the new user´s account</param>
        /// <returns>XML message response: "nok" if failed; "ok" if success</returns>
        private XDocument saveNewUserAccount(String xmlUserContent)
        {
            JChatDB db = new JChatDB();

            XDocument xmlDocument = XDocument.Parse(xmlUserContent);

            XElement user = xmlDocument.Element("frame").Element("body").Element("user");

            User userRecord = new User
            {

                nickname = user.Element("chat_name").Value.Trim(),
                email = user.Element("email").Value.Trim(),
                password = user.Element("password").Value.Trim(),
                firstName = user.Element("first_name").Value.Trim(),
                lastName = user.Element("last_name").Value.Trim(),
                birthDate = Convert.ToDateTime(user.Element("birthday").Value.Trim()),
                genderId = Convert.ToInt32(user.Element("gender_id").Value.Trim()),
                onlineStatusId = 5,
                enabled = true,
                logged = false,
                registerDate = DateTime.Now
            };

            if (!user.Element("city_id").Value.Trim().Equals("-1"))
                userRecord.cityId = Convert.ToInt32(user.Element("city_id").Value.Trim());

            XDocument xmlDoc = null;
            XElement xmlNewAccount = null;

            try
            {
                db.Users.Add(userRecord);
                db.SaveChanges();

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);

                xmlNewAccount = new XElement("response", "nok");
                xmlDoc = this.getXmlDocument("NEW_USER_ACCOUNT_RESPONSE", "", "", xmlNewAccount);

                return xmlDoc;
            }

            xmlNewAccount = new XElement("response", "ok");
            xmlDoc = this.getXmlDocument("NEW_USER_ACCOUNT_RESPONSE", "", "", xmlNewAccount);

            return xmlDoc;
        }

        /// <summary>
        /// Updates user profile´s information
        /// </summary>
        /// <param name="xmlUpdateUserContent">XML content about the user profile information update</param>
        /// <returns>XML message response: "ok" if success; "nok" if it failed</returns>
        private XDocument editUserProfile(String xmlUpdateUserContent)
        {
            JChatDB db = new JChatDB();

            XDocument xmlDocument = XDocument.Parse(xmlUpdateUserContent);

            XElement user = xmlDocument.Element("frame").Element("body").Element("user");

            int userId = Convert.ToInt32(user.Element("user_id").Value.Trim());

            XDocument xmlDoc = null;
            XElement xmlNewAccount = null;

            IEnumerable<User> allUsers = from users in db.Users
                                         where users.id == userId
                                         select users;

            User userRecord = null;

            try
            {

                userRecord = allUsers.FirstOrDefault();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);

                xmlNewAccount = new XElement("response", "nok");
                xmlDoc = this.getXmlDocument("NEW_USER_ACCOUNT_RESPONSE", "", "", xmlNewAccount);

                return xmlDoc;
            }

            userRecord.nickname = user.Element("chat_name").Value.Trim();
            userRecord.password = user.Element("password").Value.Trim();
            userRecord.firstName = user.Element("first_name").Value.Trim();
            userRecord.lastName = user.Element("last_name").Value.Trim();
            userRecord.birthDate = Convert.ToDateTime(user.Element("birthday").Value.Trim());
            userRecord.genderId = Convert.ToInt32(user.Element("gender_id").Value.Trim());
            userRecord.onlineStatusId = 5;
            userRecord.enabled = true;

            if (!user.Element("city_id").Value.Trim().Equals("-1"))
                userRecord.cityId = Convert.ToInt32(user.Element("city_id").Value.Trim());

            try
            {
                db.Entry(userRecord).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);

                xmlNewAccount = new XElement("response", "nok");
                xmlDoc = this.getXmlDocument("EDIT_USER_PROFILE_RESPONSE", "", "", xmlNewAccount);

                return xmlDoc;
            }

            xmlNewAccount = new XElement("response", "ok");
            xmlDoc = this.getXmlDocument("EDIT_USER_PROFILE_RESPONSE", "", "", xmlNewAccount);

            return xmlDoc;
        }

        /// <summary>
        /// Retrieves all users´s information for the search
        /// </summary>
        /// <param name="userSearch">Word pattern to search</param>
        /// <returns>XML message response: "null" if there are no informtion; All information for the search</returns>
        private XDocument getUserSearch(String userSearch)
        {
            JChatDB db = new JChatDB();

            IEnumerable<User> allUsers = from users in db.Users
                                         select users;

            List<User> filterUsers = allUsers.Where(u =>
            {
                if (u.firstName.IndexOf(userSearch, StringComparison.OrdinalIgnoreCase) != -1 || u.lastName.IndexOf(userSearch, StringComparison.OrdinalIgnoreCase) != -1 || u.email.IndexOf(userSearch, StringComparison.OrdinalIgnoreCase) != -1)
                    return true;
                else return false;
            }).ToList();

            XDocument xmlDoc = null;
            XElement xmlUsers = new XElement("users");

            if (filterUsers.Count() == 0)
            {
                xmlUsers.Add(new XElement("user", "null"));
                xmlDoc = this.getXmlDocument("USER_SEARCH", "", "", xmlUsers);

                return xmlDoc;
            }

            //filterUsers = filterUsers.GroupBy(u => u.id).FirstOrDefault().ToList();

            foreach (User user in filterUsers)
            {

                XElement xmlUser = new XElement("user");
                xmlUser.Add(new XElement("id", Convert.ToString(user.id)));
                xmlUser.Add(new XElement("email", user.email));
                xmlUser.Add(new XElement("nickname", user.nickname));
                xmlUser.Add(new XElement("firstname", user.firstName));
                xmlUser.Add(new XElement("lastname", user.lastName));
                xmlUser.Add(new XElement("birthdate", user.birthDate.ToString()));
                xmlUser.Add(new XElement("gender_id", Convert.ToString(user.genderId)));
                xmlUser.Add(new XElement("enabled", (user.enabled == true ? "yes" : "no")));
                xmlUser.Add(new XElement("status_id", Convert.ToString(user.onlineStatusId)));
                xmlUser.Add(new XElement("city_id", (user.cityId == null ? "null" : Convert.ToString(user.cityId))));
                xmlUsers.Add(xmlUser);
            }

            xmlDoc = this.getXmlDocument("USER_SEARCH", "", "", xmlUsers);
            return xmlDoc;
        }

        /// <summary>
        /// retrieve all friendship requests for a user
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>XML message response: "null" if there are no requests; All users profiles´ information</returns>
        private XDocument getUserFriendshipRequests(String userId)
        {
            int usrId = Convert.ToInt32(userId);

            JChatDB db = new JChatDB();

            IEnumerable<User> allUsers = from users in db.Users
                                         join requests in db.FriendshipRequests on usrId equals requests.requestedId
                                         where users.id == requests.requesterId
                                         select users;

            XDocument xmlDoc = null;
            XElement xmlUsers = new XElement("users");

            if (allUsers.Count() == 0)
            {
                xmlUsers.Add(new XElement("user", "null"));
                xmlDoc = this.getXmlDocument("USER_SEARCH", "", "", xmlUsers);

                return xmlDoc;
            }

            //filterUsers = filterUsers.GroupBy(u => u.id).FirstOrDefault().ToList();

            foreach (User user in allUsers)
            {

                XElement xmlUser = new XElement("user");
                xmlUser.Add(new XElement("id", Convert.ToString(user.id)));
                xmlUser.Add(new XElement("email", user.email));
                xmlUser.Add(new XElement("nickname", user.nickname));
                xmlUser.Add(new XElement("firstname", user.firstName));
                xmlUser.Add(new XElement("lastname", user.lastName));
                xmlUser.Add(new XElement("birthdate", user.birthDate.ToString()));
                xmlUser.Add(new XElement("gender_id", Convert.ToString(user.genderId)));
                xmlUser.Add(new XElement("enabled", (user.enabled == true ? "yes" : "no")));
                xmlUser.Add(new XElement("status_id", Convert.ToString(user.onlineStatusId)));
                xmlUser.Add(new XElement("city_id", (user.cityId == null ? "null" : Convert.ToString(user.cityId))));
                xmlUsers.Add(xmlUser);
            }

            xmlDoc = this.getXmlDocument("USER_FRIENDSHIP_REQUESTS", "", "", xmlUsers);
            return xmlDoc;
        }

        /* METHODS TO RETRIEVE DATA */

        /// <summary>
        /// Retrieve all users registered in database
        /// </summary>
        /// <returns>DataTable object with all major fields information with all the users</returns>
        public DataTable getAllUsers()
        {
            JChatDB db = new JChatDB();
            DataTable allUsers = new DataTable();

            String[] columns = { "ID", "FIRST NAME", "LAST NAME", "NICKNAME", "EMAIL ADDR.", "GENDER", "CITY", "COUNTRY", "BIRTHDATE", "ENABLED", "SIGNED ON" };

            foreach (String col in columns)
                allUsers.Columns.Add(col);

            IEnumerable<User> allUsrs = from users in db.Users
                                        select users;

            IEnumerable<Gender> allGenders = from genders in db.Genders
                                             select genders;

            IEnumerable<City> allCities = from cities in db.Cities
                                          select cities;

            IEnumerable<Country> allCountries = from countries in db.Countries
                                                select countries;

            try
            {
                List<Gender> genders = allGenders.ToList();
                List<City> cities = allCities.ToList();
                List<Country> countries = allCountries.ToList();

                foreach (User user in allUsrs)
                {
                    String city = cities.Where(ct => ct.id == user.cityId).FirstOrDefault().name;
                    int countryId = cities.Where(ct => ct.id == user.cityId).FirstOrDefault().countryId;
                    String country = countries.Where(ctr => ctr.id == countryId).FirstOrDefault().name;
                    String gender = genders.Where(g => g.id == user.genderId).FirstOrDefault().description;

                    allUsers.Rows.Add(new Object[] { Convert.ToString(user.id), user.firstName, user.lastName, user.nickname, user.email, gender, city, country, user.birthDate.ToString("dd-MM-yyyy"), (user.enabled ? "Yes" : "No"), user.registerDate.ToString("dd-MM-yyyy") });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }

            return allUsers;
        }

        /// <summary>
        /// Retrieve user profile´s information
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>User object with user profile´s information</returns>
        public User getUser(int userId)
        {
            JChatDB db = new JChatDB();
            User user = null;

            try
            {
                user = (from users in db.Users
                        where users.id == userId
                        select users).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }

            return user;
        }

        /// <summary>
        /// Retrievs all genders´ information
        /// </summary>
        /// <returns>List of Gender obects with all genders´ information</returns>
        public List<Data.Gender> getGenders()
        {
            JChatDB db = new JChatDB();
            List<Data.Gender> allGenders = new List<Data.Gender>();

            IEnumerable<Data.Gender> allGendrs = from genders in db.Genders
                                                 select genders;

            try
            {
                allGenders = allGendrs.ToList();
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }

            return allGenders;
        }

        /// <summary>
        /// Retrieves all countries´ information
        /// </summary>
        /// <returns>List of Country obects with all countries´ information</returns>
        public List<Data.Country> getCountries()
        {
            JChatDB db = new JChatDB();
            List<Data.Country> allCountries = new List<Data.Country>();

            IEnumerable<Data.Country> allCtrs = from countries in db.Countries
                                                select countries;

            try
            {
                allCountries = allCtrs.ToList();
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }

            return allCountries;
        }

        /// <summary>
        /// Retrieves all cities´ information
        /// </summary>
        /// <returns>List of City obects with all cities´ information</returns>
        public List<Data.City> getCities()
        {
            JChatDB db = new JChatDB();
            List<Data.City> allCities = new List<Data.City>();

            IEnumerable<Data.City> allCts = from cities in db.Cities
                                            select cities;

            try
            {
                allCities = allCts.ToList();
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }

            return allCities;
        }

        /// <summary>
        /// Sets "enabled" attribute of the "User" object true or false
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>False if occurred an error; True if success</returns>
        public Boolean setEnableUser(int userId)
        {
            JChatDB db = new JChatDB();

            IEnumerable<Data.User> allUsers = from users in db.Users
                                              where users.id == userId
                                              select users;

            try
            {
                Data.User user = allUsers.FirstOrDefault();

                if (user.enabled)
                    user.enabled = false;
                else if (!user.enabled)
                    user.enabled = true;

                db.Entry(user).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();

            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes a User´s profile from database
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>False if occurred an error; True if success</returns>
        public Boolean deleteUser(int userId)
        {
            JChatDB db = new JChatDB();

            IEnumerable<Data.Session> allSessions = from sessions in db.Sessions
                                                    where sessions.userId == userId
                                                    select sessions;

            IEnumerable<Data.Message> allMessages = from messages in db.Messages
                                                    where (messages.senderId == userId || messages.receiverId == userId)
                                                    select messages;

            IEnumerable<Data.Contact> allContacts = from contacts in db.Contacts
                                                    where (contacts.userId == userId || contacts.contactId == userId)
                                                    select contacts;

            IEnumerable<Data.FriendshipRequest> allRequests = from requests in db.FriendshipRequests
                                                              where (requests.requesterId == userId || requests.requestedId == userId)
                                                              select requests;

            IEnumerable<Data.User> allUsers = from users in db.Users
                                              where users.id == userId
                                              select users;

            try
            {
                Data.User user = allUsers.FirstOrDefault();

                if (allSessions.Count() > 0)
                    foreach (Data.Session session in allSessions)
                        db.Sessions.Remove(session);

                if (allMessages.Count() > 0)
                    foreach (Data.Message message in allMessages)
                        db.Messages.Remove(message);

                if (allContacts.Count() > 0)
                    foreach (Data.Contact contact in db.Contacts)
                        db.Contacts.Remove(contact);

                if (allRequests.Count() > 0)
                    foreach (Data.FriendshipRequest request in allRequests)
                        db.FriendshipRequests.Remove(request);

                db.Users.Remove(user);

                db.SaveChanges();

            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Retrieves the last login date entry for an user
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>"null" if occurred an error; Date of the last login</returns>
        public String getUserLastLogin(int userId)
        {
            JChatDB db = new JChatDB();

            String lastLogin = null;

            IEnumerable<Data.Session> allSessions = from sessions in db.Sessions
                                                    where sessions.userId == userId
                                                    orderby sessions.loginDateTime descending
                                                    select sessions;

            try
            {
                if (allSessions.Count() > 0)
                    lastLogin = allSessions.FirstOrDefault().loginDateTime.ToString("yyyy-MM-dd  hh:mm:ss");
                else lastLogin = "Never logged the Jchat application...";
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }

            return lastLogin;
        }

        /// <summary>
        /// Retrieves the number of times a user logged in the chat
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>"null" if occurred an error; Number of times a user logged in</returns>
        public int? getNumberOfTimeUserLogged(int userId)
        {
            JChatDB db = new JChatDB();

            int numberOfTimes = 0;

            IEnumerable<Data.Session> allSessions = from sessions in db.Sessions
                                                    where sessions.userId == userId
                                                    select sessions;

            try
            {
                numberOfTimes = allSessions.Count();
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }

            return numberOfTimes;
        }

        /// <summary>
        /// Retrieves the number of friends of an user
        /// </summary>
        /// <param name="userId">user´s identification number</param>
        /// <returns>"null" if occurred an error; Number of friends for the user</returns>
        public int? getUserNumberOfFriends(int userId)
        {
            JChatDB db = new JChatDB();

            int? numberOfFriends = 0;

            IEnumerable<Contact> allContacts = from contacts in db.Contacts
                                               where contacts.userId == userId
                                               select contacts;

            try
            {
                numberOfFriends = allContacts.Count();
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Utils.Utils.writeLog(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, new StackTrace(new StackFrame(true)).GetFrame(0).GetFileLineNumber().ToString(), new StackTrace(new StackFrame(true)).GetFrame(0).GetFileColumnNumber().ToString(), ex.Message);
                return null;
            }

            return numberOfFriends;
        }
    }
}
