﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JChatClient
{
    /// <summary>
    /// Window form to display information of the user
    /// </summary>
    public partial class UserProfileWindow : Form
    {
        JChatClient.Models.JChatApp app = JChatClient.Models.JChatApp.Instance();
        int userId = -1;
        Boolean isSearchedUser;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <param name="isSearchedUser">True - if is an user profile update window form; False - if is a new user´s acount</param>
        public UserProfileWindow(int userId, Boolean isSearchedUser)
        {
            this.userId = userId;
            this.isSearchedUser = isSearchedUser;
            InitializeComponent();

            if (isSearchedUser)
            {
                JChatClient.Models.User user = app.searchList.Where(u => u.id == userId).FirstOrDefault();
                this.Text = "JChat - " + user.firstName + " " + user.lastName + " profile´s information...";
                label2.Text = user.firstName + " " + user.lastName;
                label4.Text = user.nickname;
                label6.Text = user.email;
                label8.Text = "Private information";
                label10.Text = app.gendersList.Where(g => g.id == user.genderId).FirstOrDefault().description;
                label12.Text = "Private information...";
                label14.Text = (user.cityId != -1 ? app.citiesList.Where(c => c.id == user.cityId).FirstOrDefault().name + ", " + app.countriesList.Where(cr => cr.id == app.citiesList.Where(t => t.id == user.cityId).FirstOrDefault().countryId).FirstOrDefault().name : "No city, no country...");
                label16.Text = "This user does not belong to your contacts...";
                button1.Show();
            }
            else if (!isSearchedUser)
            {
                JChatClient.Models.Contact user = app.contactsList.Where(u => u.id == userId).FirstOrDefault();
                this.Text = "JChat - " + user.firstName + " " + user.lastName + " profile´s information...";
                label2.Text = user.firstName + " " + user.lastName;
                label4.Text = user.nickname;
                label6.Text = user.email;
                label8.Text = user.birthdate.ToString("dd-MMM-yyyy");
                label10.Text = app.gendersList.Where(g => g.id == user.genderId).FirstOrDefault().description;

                if (user.onlineStatusId == 3 || user.onlineStatusId == 4)
                    label12.Text = "Offline";
                else label12.Text = app.statusList.Where(o => o.id == user.onlineStatusId).FirstOrDefault().descripton;

                label14.Text = (user.cityId != -1 ? app.citiesList.Where(c => c.id == user.cityId).FirstOrDefault().name + ", " + app.countriesList.Where(cr => cr.id == app.citiesList.Where(t => t.id == user.cityId).FirstOrDefault().countryId).FirstOrDefault().name : "No city, no country...");
                label16.Text = user.connectionDateTime.ToString("dd-MMM-yyyy");
            }

        }

        /// <summary>
        /// Button´s click event handler to request friendship
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            String requesterId = Convert.ToString(this.app.profile.id);
            String requestedId = Convert.ToString(this.userId);

            Task.Factory.StartNew(() => {

                String response = this.app.requestFriendship(requesterId, requestedId);

                if (response.Equals("nack"))
                {
                    MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                    return;
                }
            });
            this.Dispose();
        }
    }
}
