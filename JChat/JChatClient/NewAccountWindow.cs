﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JChatClient
{
    /// <summary>
    /// Window form to create a new user account
    /// </summary>
    public partial class NewAccountWindow : Form
    {
        Form parent = null;
        Models.JChatApp app = Models.JChatApp.Instance();
        Boolean isNewAccount;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parent">Parent window form</param>
        /// <param name="isNewAccount">True - if is an user profile update window form; False - if is a new user´s acount</param>
        public NewAccountWindow(Form parent, Boolean isNewAccount)
        {
            this.parent = parent;
            this.isNewAccount = isNewAccount;
            parent.Hide();

            InitializeComponent();

            String response = this.app.updateGendersFromServer();

            if (response.Equals("nack"))
            {
                MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                this.parent.Show();
                this.Dispose();

                return;
            }

            response = this.app.updateCountriesFromServer();

            if (response.Equals("nack"))
            {
                MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                this.parent.Show();
                this.Dispose();

                return;
            }

            response = this.app.updateCitiesFromServer();

            if (response.Equals("nack"))
            {
                MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                this.parent.Show();
                this.Dispose();

                return;
            }

            comboBox1.DataSource = new BindingSource(app.getAllGendersData(), null);
            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";
            comboBox1.SelectedIndex = 0;

            comboBox3.DataSource = new BindingSource(app.getAllCountriesData(), null);
            comboBox3.DisplayMember = "Value";
            comboBox3.ValueMember = "Key";
            comboBox3.SelectedIndex = 0;

            if (!isNewAccount)
            {
                this.Text = "JChat - Edit account...";
                button1.Text = "Save profile >>";
                button2.Text = "<< Back";

                textBox1.Text = this.app.profile.nickname;
                label2.Hide();
                textBox2.Hide();
                textBox5.Text = this.app.profile.firstName;
                textBox6.Text = this.app.profile.lastName;
                dateTimePicker1.Value = this.app.profile.birthdate;

                foreach (var gender in comboBox1.Items)
                {
                    int key = Convert.ToInt32(gender.ToString().Split('[')[1].Split(',')[0]);

                    if (key == this.app.profile.genderId)
                    {

                        comboBox1.SelectedItem = gender;
                        break;
                    }
                }

                int countryId = this.app.getCountryIdByCityId(this.app.profile.cityId);

                foreach (var country in comboBox3.Items)
                {
                    int key = Convert.ToInt32(country.ToString().Split('[')[1].Split(',')[0]);

                    if (key == countryId)
                    {
                        comboBox3.SelectedItem = country;
                        break;
                    }
                }
            }

            comboBox2.DataSource = new BindingSource(app.getAllCitiesByCountryIdData(Convert.ToInt32(comboBox3.SelectedValue)), null);
            comboBox2.DisplayMember = "Value";
            comboBox2.ValueMember = "Key";

            if (!isNewAccount)
            {
                foreach (var city in comboBox2.Items)
                {
                    int key = Convert.ToInt32(city.ToString().Split('[')[1].Split(',')[0]);

                    if (key == this.app.profile.cityId)
                    {
                        comboBox2.SelectedItem = city;
                        break;
                    }
                }
            }

            this.Show();
        }

        /// <summary>
        /// Window form closed´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewAccountWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.parent.Show();
            this.Dispose();
        }

        /// <summary>
        /// Button´s click event handler to close the window form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            this.parent.Show();
            this.Dispose();
        }

        /// <summary>
        /// Gets local ip address
        /// </summary>
        /// <returns>Ip address</returns>
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        /// <summary>
        /// Button´s click event handler to save new user´s account
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            String confirmation = "";
            String title = "";

            if (isNewAccount)
            {
                confirmation = "Save your new account?";
                title = "New account confirmation...";
            }
            else
            {
                confirmation = "Save your account´s changes?";
                title = "Account´s changes confirmation...";
            }

            DialogResult result = MessageBox.Show(this, confirmation, title, MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                String chatName = textBox1.Text;
                String email = textBox2.Text;
                String password = textBox3.Text;
                String confPassword = textBox4.Text;
                String firstName = textBox5.Text;
                String lastName = textBox6.Text;
                DateTime birthday = dateTimePicker1.Value;
                String gender_id = Convert.ToString(comboBox1.SelectedValue);
                String country_id = Convert.ToString(comboBox3.SelectedValue);
                String city_id = Convert.ToString(comboBox2.SelectedValue);

                Dictionary<String, String> validations = Validations.Validation.validateNewUserAccount(chatName, email, password, confPassword, firstName, lastName, birthday);

                String emtyFields = "<REQUIRED FIELDS>\n\n\t";
                String invalidFields = "<INVALID FIELDS>\n\n\t";

                if (validations.ContainsKey("chatName") && validations["chatName"].Equals("empty"))
                    emtyFields += ">> 'NICKENAME'\n\t";

                if (isNewAccount)
                {
                    if (validations.ContainsKey("email") && validations["email"].Equals("empty"))
                        emtyFields += ">> 'EMAIL'\n\t";

                    if (validations.ContainsKey("email") && validations["email"].Equals("nvalid"))
                        invalidFields += ">> 'EMAIL IS NOT VALID...'\n\t";

                    if (validations.ContainsKey("email") && validations["email"].Equals("exists"))
                        invalidFields += ">> 'EMAIL ALREADY EXISTS...'\n\t";
                }
                else if (validations.ContainsKey("email"))
                {
                    validations.Remove("email");
                }

                if (validations.ContainsKey("password") && validations["password"].Equals("empty"))
                    emtyFields += ">> 'PASSWORD'\n\t";

                if (validations.ContainsKey("password") && validations["password"].Equals("length"))
                    invalidFields += ">> 'PASSWORD MUST HAVE A NIMUM OF 8 CHARACTERES...'\n\t";

                if (validations.ContainsKey("confPassword") && validations["confPassword"].Equals("empty"))
                    emtyFields += ">> 'CONFIRM PASSWORD'\n\t";

                if (validations.ContainsKey("firstName") && validations["firstName"].Equals("empty"))
                    emtyFields += ">> 'FIRST NAME'\n\t";

                if (validations.ContainsKey("lastName") && validations["lastName"].Equals("empty"))
                    emtyFields += ">> 'LAST NAME'\n\t";

                if (validations.ContainsKey("birthDate") && validations["birthDate"].Equals("nvalid"))
                    invalidFields += ">> 'BIRTHDATE MUST BE EARLIER THAN THE CURRENT DATE...'\n\t";

                if (validations.Count() > 0)
                {
                    String errors = emtyFields + "\n\n" + invalidFields;
                    MessageBox.Show(this, errors, "User´s account errors...");

                    return;
                }

                if (isNewAccount)
                {
                    String response = this.app.saveNewUserResponse(chatName, email, password, firstName, lastName, birthday, gender_id, country_id, city_id);

                    if (!response.Equals("nack") && !response.Equals("nok"))
                    {
                        MessageBox.Show(this, "Your new account was successful registered!\n\nGo to the sign form", "Registration result...");

                        textBox1.Text = String.Empty;
                        textBox2.Text = String.Empty;
                        textBox3.Text = String.Empty;
                        textBox4.Text = String.Empty;
                        textBox5.Text = String.Empty;
                        textBox6.Text = String.Empty;
                    }
                    else MessageBox.Show(this, "Occurred an error while saving your new account!\n\nPlease try again...", "Registration result...");
                }
                else
                {
                    String userId = Convert.ToString(this.app.profile.id);
                    String response = this.app.editUserProfileResponse(userId, chatName, password, firstName, lastName, birthday, gender_id, country_id, city_id);

                    if (!response.Equals("nack") && !response.Equals("nok"))
                    {
                        MessageBox.Show(this, "Your profile has been modified with success!", "Edition result...");

                        this.app.updateProfileFromServer(this.app.profile.email);
                        this.parent.Show();
                        this.Dispose();
                    }
                    else MessageBox.Show(this, "Occurred an error while saving the changes!\n\nPlease try again...", "Edition result...");
                }
            }
        }

        /// <summary>
        /// Countries combo box selection change´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox3_SelectionChangeCommitted(object sender, EventArgs e)
        {
            comboBox2.DataSource = new BindingSource(this.app.getAllCitiesByCountryIdData(Convert.ToInt32(comboBox3.SelectedValue)), null);
            comboBox2.DisplayMember = "Value";
            comboBox2.ValueMember = "Key";
            comboBox2.SelectedIndex = 0;
        }
    }
}
