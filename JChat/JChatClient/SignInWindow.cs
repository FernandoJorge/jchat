﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JChatClient
{
    /// <summary>
    /// Window form - Chat Main Form
    /// </summary>
    public partial class SignInWindow : Form
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SignInWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Button´s event handler to display new user´s account window form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            NewAccountWindow newAccount = new NewAccountWindow(this, true);
        }

        /// <summary>
        /// Button´s event handler to authenticate user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            String email = textBox1.Text;
            String password = textBox2.Text;

            Dictionary<String, String> errors = Validations.Validation.validateLoginAccount(email, password);

            String emptyFields = "<REQUIRED FIELDS>\n\n\t";
            String invalidFields = "<INVALID FIELDS>\n\n\t";

            if (errors.Count() > 0)
            {
                if (errors.ContainsKey("email"))
                {
                    if (errors["email"].Equals("empty"))
                        emptyFields += ">> 'EMAIL'\n\t";

                    if (errors["email"].Equals("nvalid"))
                        invalidFields += ">> 'INVALID EMAIL ADDRESS'\n\t";
                }

                if (errors.ContainsKey("password"))
                {

                    if (errors["password"].Equals("empty"))
                        emptyFields += ">> 'PASSWORD'\n\t";

                    if (errors["password"].Equals("length"))
                        invalidFields += ">> 'PASSWORD MUST HAVE AT LEAST 8 CHARACTERS'\n\t";
                }

                String allErrors = emptyFields + "\n\n" + invalidFields;

                MessageBox.Show(this, allErrors, "Login errors...");

                return;
            }

            Models.JChatApp app = Models.JChatApp.Instance();

            // Authenticate user
            if (app.authenticateUser(email, password).Equals("nack"))
            {
                MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                return;
            }
            else if (app.authenticateUser(email, password).Equals("nok"))
            {
                textBox2.Text = String.Empty;
                MessageBox.Show(this, "Wrong credentials...");

                return;
            }

            // Clears fields
            textBox1.Text = String.Empty;
            textBox2.Text = String.Empty;

            // Updates all application´s data
            app.updateProfileEmail(email);

            String response = app.updateProfileFromServer(app.profile.email);

            if (response.Equals("nack"))
            {
                MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                return;
            }

            response = app.updateCountriesFromServer();

            if (response.Equals("nack"))
            {
                MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                return;
            }

            response = app.updateCitiesFromServer();

            if (response.Equals("nack"))
            {
                MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                return;
            }

            response = app.updateGendersFromServer();

            if (response.Equals("nack"))
            {
                MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                return;
            }

            response = app.updateStatusFromServer();

            if (response.Equals("nack"))
            {
                MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                return;
            }

            UserWindow userWindow = new UserWindow(this);
        }
    }
}
