﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JChatClient
{
    /// <summary>
    /// Window form for all of the users that requested friendship
    /// </summary>
    public partial class FriendRequestsWindow : Form
    {
        JChatClient.Models.JChatApp app = JChatClient.Models.JChatApp.Instance();
        DataTable dtSource;

        /// <summary>
        /// Constructor
        /// </summary>
        public FriendRequestsWindow()
        {
            InitializeComponent();

            dtSource = this.app.getAllFriendshipRequestsData();
            dtSource.Columns[0].ColumnMapping = MappingType.Hidden;
            dataGridView1.DataSource = dtSource;

            DataGridViewButtonColumn bt = new DataGridViewButtonColumn();
            bt.Text = "Accept friendhip...";
            bt.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(bt);
        }

        /// <summary>
        /// Button´s click event handler to accept friendship request
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            var gridView = (DataGridView)sender;

            if (gridView.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                DialogResult result = MessageBox.Show(this, "Confirm user as new contact?", "Contact confirmation...", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    int userId = Convert.ToInt32(dtSource.Rows[e.RowIndex][0].ToString());

                    Task.Factory.StartNew(() => {
                        
                        String response = this.app.requestFriendshipAcceptance(Convert.ToString(userId), Convert.ToString(this.app.profile.id));

                        if(response.Equals("nack"))
                            MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                    });
                    this.Dispose();
                }
            }
        }
    }
}
