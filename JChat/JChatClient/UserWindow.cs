﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JChatClient
{
    /// <summary>
    /// Window form for the the authenticated user
    /// </summary>
    public partial class UserWindow : Form
    {
        Form parent;
        Models.JChatApp app = null;
        Boolean searchMode;
        CancellationTokenSource tokenSource;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parent">Parent window form object</param>
        public UserWindow(Form parent)
        {
            this.parent = parent;
            parent.Hide();
            this.app = Models.JChatApp.Instance();

            tokenSource = new CancellationTokenSource();

            InitializeComponent();

            this.Text = "JChat - " + app.profile.nickname;

            label1.Text = app.profile.nickname;
            label2.Text = app.getUserOnlineStatusData();

            comboBox1.DataSource = new BindingSource(app.getAllOnlineStatusData(), null);
            comboBox1.DisplayMember = "Value";
            comboBox1.ValueMember = "Key";
            comboBox1.SelectedIndex = 0;
            comboBox1.Hide();

            comboBox2.SelectedIndex = 0;

            this.Show();
        }

        /// <summary>
        /// Updates current user´s conversation
        /// </summary>
        /// <param name="tokenSource">Update conversation´s cancellation signal</param>
        private void updateCurrentUserContactConversation(CancellationTokenSource tokenSource)
        {

            do
            {
                try
                {
                    int ctId = -1;

                    listBox1.Invoke(new MethodInvoker(() =>
                    {
                        KeyValuePair<String, String> selectedProperty = (KeyValuePair<String, String>)listBox1.SelectedItem;
                        ctId = Convert.ToInt32(selectedProperty.Key);
                    }));

                    label3.Invoke(new MethodInvoker(() => label3.Text = this.app.getContactNameData(ctId, this.searchMode)));

                    label4.Invoke(new MethodInvoker(() =>
                    {
                        String contactStatus = this.app.getContactOnlineStatusData(ctId, this.searchMode);
                        label4.Text = contactStatus;
                    }));

                    if (this.app.contactsStatus.Equals("Offline") || this.app.getContactOnlineStatusData(ctId, this.searchMode).Equals("Offline") || this.app.getContactOnlineStatusData(ctId, this.searchMode).Equals("Offline"))
                    {
                        richTextBox1.Invoke(new MethodInvoker(() =>
                        {
                            button1.Enabled = false;
                            richTextBox1.Enabled = false;
                        }));
                    }
                    else
                    {
                        richTextBox1.Invoke(new MethodInvoker(() =>
                        {
                            button1.Enabled = true;
                            richTextBox1.Enabled = true;
                        }));
                    }

                    List<Models.Message> conversation = this.app.getConversationData(ctId);

                    if (conversation != null && conversation.Count() > 0)
                    {

                        richTextBox2.Invoke(new MethodInvoker(() =>
                        {

                            StringBuilder allMessages = new StringBuilder();
                            richTextBox2.Clear();

                            foreach (Models.Message message in conversation)
                            {

                                allMessages.AppendLine(message.dateTime.ToString("<dddd, HH:mm:ss, dd-MM-yyyy>"));

                                if (message.senderId == this.app.profile.id)
                                    allMessages.AppendLine("I wrote,");
                                else allMessages.AppendLine(this.app.getContactNameData(ctId, this.searchMode) + " wrote,");

                                allMessages.AppendLine();
                                allMessages.AppendLine(message.message);
                                allMessages.AppendLine();
                            }

                            richTextBox2.Text = allMessages.ToString();
                            richTextBox2.Select(richTextBox2.Text.Length - 1, 0);
                            richTextBox2.ScrollToCaret();
                        }));
                    }
                    else richTextBox2.Text = String.Empty;
                }
                catch (Exception ex)
                {
                    continue;
                }

                Console.WriteLine();
                Console.WriteLine("<< USER CONVERSATION UPDATED >> " + DateTime.Now.ToString());
                Console.WriteLine();
                Thread.Sleep(1000);
            } while (!tokenSource.IsCancellationRequested);
        }

        /// <summary>
        /// Updates all user´s messages of all conversation
        /// </summary>
        /// <param name="tokenSource">Update messages´ cancellation signal</param>
        /// <returns></returns>
        private String updateUserMessages(CancellationTokenSource tokenSource)
        {
            String response;

            do
            {
                response = this.app.updateMessagesFromServer(app.profile.id);

                if (response.Equals("nack"))
                    return "nack";

                Console.WriteLine();
                Console.WriteLine("<< USER´S MESSAGES UPDATED >> " + DateTime.Now.ToString());
                Console.WriteLine();
                Thread.Sleep(2000);
            } while (!tokenSource.IsCancellationRequested);

            return "ack";
        }

        /// <summary>
        /// Updates user´s contacts list box
        /// </summary>
        /// <param name="tokenSource">Update contact´s list box cancellation signal</param>
        /// <returns></returns>
        private String updateContactListBox(CancellationTokenSource tokenSource)
        {
            JChatClient.Models.JChatApp app = JChatClient.Models.JChatApp.Instance();
            String response;

            do
            {
                if (!this.searchMode)
                {
                    response = app.updateContactsFromServer(app.profile.id);

                    if (response.Equals("nack"))
                        return "nack";

                    if (app.getAllUserContactsData(this.searchMode, this.app.contactsStatus).Count() > 0)
                    {
                        listBox1.Invoke(new MethodInvoker(() =>
                        {
                            listBox1.DataSource = null;
                            listBox1.DataSource = new BindingSource(app.getAllUserContactsData(this.searchMode, this.app.contactsStatus), null);
                            listBox1.DisplayMember = "Value";
                            listBox1.ValueMember = "key";
                            listBox1.SelectedIndex = this.app.ListBox1LastIndex;
                        }));
                    }

                    Console.WriteLine();
                    Console.WriteLine("<< USER´S CONTACT LIST UPDATED >> " + DateTime.Now.ToString());
                    Console.WriteLine();
                    Thread.Sleep(5000);
                }
            } while (!tokenSource.IsCancellationRequested);

            return "ack";
        }

        /// <summary>
        /// Updates friendship request alert
        /// </summary>
        /// <param name="tokenSource">Update alert´s cancellation signal</param>
        private void updateFriendshipRequestsWarningLabel(CancellationTokenSource tokenSource)
        {
            JChatClient.Models.JChatApp app = JChatClient.Models.JChatApp.Instance();

            do
            {

                label8.Invoke(new MethodInvoker(() =>
                {

                    if (app.friendshipRequestsList != null && app.friendshipRequestsList.Count() > 0)
                    {
                        label8.Text = "Friend requests!!!";
                        label8.ForeColor = Color.Red;
                    }
                    else
                    {
                        label8.Text = "No friend requests";
                        label8.ForeColor = Color.Black;
                    }
                }));

                Console.WriteLine();
                Console.WriteLine("<< USER´S FRIENDSHIPS REQUESTS UPDATED >> " + DateTime.Now.ToString());
                Console.WriteLine();
                Thread.Sleep(5000);
            } while (!tokenSource.IsCancellationRequested);
        }

        /// <summary>
        /// Window form closed´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!this.app.userLogout(app.profile.id).Equals("nack"))
            {
                this.tokenSource.Cancel();
                this.app = null;
                this.parent.Show();
                this.Dispose();
                GC.Collect();
            }
            else
            {
                MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                return;
            }
        }

        /// <summary>
        /// User sign out button´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void signOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.app.userLogout(app.profile.id).Equals("nack"))
            {
                this.tokenSource.Cancel();
                this.app = null;
                this.parent.Show();
                this.Dispose();
                GC.Collect();
            }
            else
            {
                MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                return;
            }
        }

        /// <summary>
        /// Online status combo box selection change´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int statusId = Convert.ToInt32(comboBox1.SelectedValue);

            this.app.setOnlineStatusData(statusId);
            label2.Text = this.app.getUserOnlineStatusData();
            comboBox1.Hide();

            Task.Factory.StartNew(() =>
            {

                String response = this.app.changeOnlineStatus(Convert.ToString(this.app.profile.id), Convert.ToString(statusId));

                if (response.Equals("nack"))
                {
                    MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                    return;
                }
            });
        }

        /// <summary>
        /// Online status description click´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label2_Click(object sender, EventArgs e)
        {
            if (!comboBox1.Visible)
                comboBox1.Show();
            else if (comboBox1.Visible)
                comboBox1.Hide();
        }

        /// <summary>
        /// Send message button click´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            KeyValuePair<String, String> selectedProperty;

            try
            {
                selectedProperty = (KeyValuePair<String, String>)listBox1.SelectedItem;
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show(this, "Select contact to send message.");
                return;
            }



            int receiverId = Convert.ToInt32(selectedProperty.Key);

            String message = richTextBox1.Text;

            if (!String.IsNullOrEmpty(message))
            {
                Task.Factory.StartNew(() =>
                {

                    String response = this.app.sendMessageToUser(Convert.ToString(this.app.profile.id), Convert.ToString(receiverId), message);

                    if (response.Equals("nack"))
                        MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                });
            }

            richTextBox1.Text = String.Empty;
        }

        /// <summary>
        /// Search button click´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            panel4.Hide();
            String search = textBox1.Text;

            if (!String.IsNullOrEmpty(search))
            {
                this.searchMode = true;
                String response = this.app.searchUser(search);

                if (response.Equals("nack"))
                {
                    MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                    return;
                }

                if (app.getSearchData().Count() > 0)
                {
                    listBox1.DataSource = null;
                    listBox1.DataSource = new BindingSource(app.getSearchData(), null);
                    listBox1.DisplayMember = "Value";
                    listBox1.ValueMember = "key";
                }
            }

            label6.Text = "Search results...";
        }

        /// <summary>
        /// Search text box change´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            if (!String.IsNullOrEmpty(textBox1.Text))
                button2.Enabled = true;
            else button2.Enabled = false;

            if (String.IsNullOrEmpty(textBox1.Text))
            {
                if (app.getAllUserContactsData(this.searchMode).Count() > 0)
                {
                    listBox1.DataSource = new BindingSource(app.getAllUserContactsData(this.searchMode), null);
                    listBox1.DisplayMember = "Value";
                    listBox1.ValueMember = "key";
                }

                this.searchMode = false;
                label6.Text = "Contacts:";
            }
        }

        /// <summary>
        /// User´s contact list mouse down´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_MouseDown(object sender, MouseEventArgs e)
        {
            int indexOver = listBox1.IndexFromPoint(e.X, e.Y);

            if (e.Button == MouseButtons.Right)
            {
                if (indexOver >= 0)
                {
                    this.app.ListBox1LastIndex = indexOver;
                    listBox1.SelectedIndex = indexOver;

                    if (this.searchMode)
                        label9.Hide();
                    else if (!this.searchMode)
                        label9.Show();

                    panel4.Location = new Point(e.X, e.Y);
                    panel4.Show();
                }
            }

            if (e.Button == MouseButtons.Left)
            {

                if (this.searchMode)
                    return;

                if (indexOver >= 0)
                {
                    this.app.ListBox1LastIndex = indexOver;

                    try
                    {
                        KeyValuePair<String, String> selectedProperty = (KeyValuePair<String, String>)listBox1.SelectedItem;
                        int ctId = Convert.ToInt32(selectedProperty.Key);

                        label3.Text = this.app.getContactNameData(ctId, this.searchMode);
                        int userStatusId = this.app.profile.onlineStatusId;
                        String contactStatus = this.app.getContactOnlineStatusData(ctId, this.searchMode);
                        label4.Text = contactStatus;

                        if (contactStatus.Equals("Offline") || userStatusId == 3 || userStatusId == 4)
                        {
                            button1.Enabled = false;
                            richTextBox1.Enabled = false;
                        }
                        else
                        {
                            button1.Enabled = true;
                            richTextBox1.Enabled = true;
                        }

                        List<Models.Message> conversation = this.app.getConversationData(ctId);

                        if (conversation != null && conversation.Count() > 0)
                        {
                            StringBuilder allMessages = new StringBuilder();
                            richTextBox2.Clear();

                            foreach (Models.Message message in conversation)
                            {

                                allMessages.AppendLine(message.dateTime.ToString("<dddd, HH:mm:ss, dd-MM-yyyy>"));

                                if (message.senderId == this.app.profile.id)
                                    allMessages.AppendLine("I wrote,");
                                else allMessages.AppendLine(this.app.getContactNameData(ctId, this.searchMode) + " wrote,");

                                allMessages.AppendLine();
                                allMessages.AppendLine(message.message);
                                allMessages.AppendLine();
                            }

                            richTextBox2.Text = allMessages.ToString();
                            richTextBox2.Select(richTextBox2.Text.Length - 1, 0);
                            richTextBox2.ScrollToCaret();
                        }
                        else richTextBox2.Text = String.Empty;
                    }
                    catch (Exception ex)
                    {

                        // Continue
                    }
                }
                else panel4.Hide();
            }
        }

        /// <summary>
        /// Label7 View contact profile click´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label7_Click(object sender, EventArgs e)
        {
            int userId = Convert.ToInt32(listBox1.SelectedValue);
            panel4.Hide();
            UserProfileWindow userProfile = new UserProfileWindow(userId, this.searchMode);
            userProfile.ShowDialog(this);
        }

        /// <summary>
        /// label8 click´s event handler to show window form with friendship users requests
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label8_Click(object sender, EventArgs e)
        {
            if (this.app.friendshipRequestsList != null && this.app.friendshipRequestsList.Count() > 0)
            {
                FriendRequestsWindow requestsWin = new FriendRequestsWindow();
                requestsWin.ShowDialog(this);
            }
        }

        /// <summary>
        /// Label6 click´s event handler to make (in)visible combo box 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label6_Click(object sender, EventArgs e)
        {
            if (label6.Text.Equals("Contacts:"))
                if (!comboBox2.Visible)
                    comboBox2.Show();
                else if (comboBox2.Visible)
                    comboBox2.Hide();
        }

        /// <summary>
        /// Combo box 2 selection change´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            richTextBox2.Clear();
            int index = comboBox2.SelectedIndex;
            comboBox2.Hide();

            if (index == 0)
            {
                listBox1.DataSource = new BindingSource(app.getAllUserContactsData(this.searchMode), null);
                this.app.contactsStatus = "all";
            }

            if (index == 1)
            {
                listBox1.DataSource = new BindingSource(app.getAllUserContactsData(this.searchMode, "online"), null);
                this.app.contactsStatus = "online";
            }

            listBox1.DisplayMember = "Value";
            listBox1.ValueMember = "key";
            this.app.ListBox1LastIndex = 0;
        }

        /// <summary>
        /// Window form show´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserWindow_Shown(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                String response = this.updateUserMessages(this.tokenSource);

                if (response.Equals("nack"))
                {
                    this.Invoke(new MethodInvoker(() => MessageBox.Show(this, "Failed to connect to server...", "Connetion server...")));
                }
            }, tokenSource.Token);

            Task.Factory.StartNew(() =>
            {
                String response = this.app.updateFriendshipRequests(this.tokenSource);

                if (response.Equals("nack"))
                {
                    this.Invoke(new MethodInvoker(() => MessageBox.Show(this, "Failed to connect to server...", "Connetion server...")));
                }
            }, tokenSource.Token);

            Task.Factory.StartNew(() =>
            {
                String response = this.updateContactListBox(this.tokenSource);

                if (response.Equals("nack"))
                {
                    this.Invoke(new MethodInvoker(() => MessageBox.Show(this, "Failed to connect to server...", "Connetion server...")));
                }
            }, tokenSource.Token);

            Task.Factory.StartNew(() =>
            {
                this.updateFriendshipRequestsWarningLabel(this.tokenSource);
            }, tokenSource.Token);

            Task.Factory.StartNew(() =>
            {
                this.updateCurrentUserContactConversation(this.tokenSource);
            }, tokenSource.Token);
        }

        /// <summary>
        /// Label3 double click´s event handler to show contact profile information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label3_DoubleClick(object sender, EventArgs e)
        {
            int userId = Convert.ToInt32(listBox1.SelectedValue);
            panel4.Hide();
            UserProfileWindow userProfile = new UserProfileWindow(userId, false);
            userProfile.ShowDialog(this);
        }

        /// <summary>
        /// Menu item "away" click´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void awayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.app.setOnlineStatusData(1);
            label2.Text = this.app.getUserOnlineStatusData();
            comboBox1.SelectedIndex = 0;
            Task.Factory.StartNew(() =>
            {

                String response = this.app.changeOnlineStatus(Convert.ToString(this.app.profile.id), "1");

                if (response.Equals("nack"))
                {
                    MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                    return;
                }
            });
        }

        /// <summary>
        /// Menu item "busy" click´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void busyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.app.setOnlineStatusData(2);
            label2.Text = this.app.getUserOnlineStatusData();
            comboBox1.SelectedIndex = 1;
            Task.Factory.StartNew(() =>
            {
                String response = this.app.changeOnlineStatus(Convert.ToString(this.app.profile.id), "2");

                if (response.Equals("nack"))
                {
                    MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                    return;
                }
            });
        }

        /// <summary>
        /// Menu item "invisible" click´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void invisibleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.app.setOnlineStatusData(3);
            label2.Text = this.app.getUserOnlineStatusData();
            comboBox1.SelectedIndex = 2;
            Task.Factory.StartNew(() =>
            {
                String response = this.app.changeOnlineStatus(Convert.ToString(this.app.profile.id), "3");

                if (response.Equals("nack"))
                {
                    MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                    return;
                }
            });
        }

        /// <summary>
        /// Menu item "offline" click´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void offlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.app.setOnlineStatusData(4);
            label2.Text = this.app.getUserOnlineStatusData();
            comboBox1.SelectedIndex = 3;
            Task.Factory.StartNew(() =>
            {

                String response = this.app.changeOnlineStatus(Convert.ToString(this.app.profile.id), "4");

                if (response.Equals("nack"))
                {
                    MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                    return;
                }
            });
        }

        /// <summary>
        /// Menu item "online" click´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.app.setOnlineStatusData(5);
            label2.Text = this.app.getUserOnlineStatusData();
            comboBox1.SelectedIndex = 4;
            Task.Factory.StartNew(() =>
            {

                String response = this.app.changeOnlineStatus(Convert.ToString(this.app.profile.id), "5");

                if (response.Equals("nack"))
                {
                    MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                    return;
                }
            });
        }

        /// <summary>
        /// label9 click´s event handler to delete user´s contact
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label9_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(this, "Confirm contact delettion?", "Contact deletion...", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                try
                {
                    panel4.Hide();

                    KeyValuePair<String, String> selectedProperty = (KeyValuePair<String, String>)listBox1.SelectedItem;
                    int ctId = Convert.ToInt32(selectedProperty.Key);

                    Task.Factory.StartNew(() =>
                    {

                        String response = this.app.deleteContact(Convert.ToString(this.app.profile.id), Convert.ToString(ctId));

                        if (response.Equals("nack"))
                        {
                            MessageBox.Show(this, "Failed to connect to server...", "Connection to server...");
                            return;
                        }
                    });

                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, "Select a contact...", "Error...");
                    return;
                }
            }
        }

        /// <summary>
        /// Menu item "edit your profile..." click´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editYourProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewAccountWindow editProfileWindow = new NewAccountWindow(this, false);
        }

        /// <summary>
        /// Window form visibility change´s event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserWindow_VisibleChanged(object sender, EventArgs e)
        {
            label1.Text = this.app.profile.nickname;
        }
    }
}
