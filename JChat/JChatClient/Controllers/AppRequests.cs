﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JChatClient.Controllers
{
    class AppRequests
    {
        /// <summary>
        /// Send a request to server
        /// </summary>
        /// <param name="host">Id address of the server</param>
        /// <param name="port">Port number of the server</param>
        /// <param name="message">Request message - XML</param>
        /// <param name="waitResponse">Wait for response of the server - true; Don´t wait for response of the server - false</param>
        /// <returns>XML response type from the server or string type</returns>
        public String sendRequestToServer(String host, int port, String message, Boolean waitResponse)
        {
            Data.ClientSockets.AsynchronousClient client = new Data.ClientSockets.AsynchronousClient(host, port);

            CancellationTokenSource tokenSource = new CancellationTokenSource();
            CancellationToken token = tokenSource.Token;
            int timeOut = 3000; // 3 seconds

            Task connection = Task.Factory.StartNew(() => client.ConnectToRemoteHost(), token);

            if (!connection.Wait(timeOut, token))
                return "nack";

            String response = client.sendMessageToServer(message, waitResponse);

            return response;
        }
    }
}
