﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Sockets;


namespace JChatClient.Validations
{
    static class Validation
    {
        /// <summary>
        /// Validates new user´s account data
        /// </summary>
        /// <param name="chatName">New user´s nickname</param>
        /// <param name="email">New user´s email address</param>
        /// <param name="password">New user´s password</param>
        /// <param name="confPassaword">New user´s confirmation password</param>
        /// <param name="firstName">New user´s first name</param>
        /// <param name="lastName">New user´s last name</param>
        /// <param name="birthDate">New user´s birthday</param>
        /// <returns>Dictionary object with errors for all fields</returns>
        public static Dictionary<String, String> validateNewUserAccount(String chatName, String email, String password, String confPassaword, String firstName, String lastName, DateTime birthDate)
        {
            Dictionary<String, String> errors = new Dictionary<String, String>();
            Boolean[] passwords = new Boolean[2];

            if (String.IsNullOrEmpty(chatName))
                errors["chatName"] = "empty";

            if (String.IsNullOrEmpty(email))
                errors["email"] = "empty";
            else if (!new EmailAddressAttribute().IsValid(email))
                errors["email"] = "nvalid";
            else
            {
                Models.JChatApp app = Models.JChatApp.Instance();

                if (app.existsEmail(email).Equals("nack") || app.existsEmail(email).Equals("exists"))
                    errors["email"] = "exists";
            }

            if (String.IsNullOrEmpty(password))
                errors["password"] = "empty";
            else if (password.Length < 8)
                errors["password"] = "length";
            else passwords[0] = true;

            if (String.IsNullOrEmpty(confPassaword))
                errors["confPassword"] = "empty";
            else passwords[1] = true;

            if (!passwords.Contains(false))
                if (!password.Equals(confPassaword))
                    errors["confPassword"] = "nmatch";

            if (String.IsNullOrEmpty(firstName))
                errors["firstName"] = "empty";

            if (String.IsNullOrEmpty(lastName))
                errors["lastName"] = "empty";

            if (birthDate.Date.CompareTo(DateTime.Today.Date) > 0)
                errors["birthDate"] = "nvalid";

            return errors;
        }

        /// <summary>
        /// Validates user´s authentication credentials
        /// </summary>
        /// <param name="email">User´s email address</param>
        /// <param name="password">User´s password</param>
        /// <returns>Dictionary object with erros of all fields</returns>
        public static Dictionary<String, String> validateLoginAccount(String email, String password)
        {
            Dictionary<String, String> errors = new Dictionary<String, String>();

            if (String.IsNullOrEmpty(email))
                errors["email"] = "empty";
            else if (!new EmailAddressAttribute().IsValid(email))
                errors["email"] = "nvalid";

            if (String.IsNullOrEmpty(password))
                errors["password"] = "empty";
            else if (password.Length < 8)
                errors["password"] = "length";

            return errors;
        }

        /// <summary>
        /// Gets local ip address
        /// </summary>
        /// <returns>Ip address</returns>
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
