﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JChatClient.Models
{
    /// <summary>
    /// Model for a city
    /// </summary>
    public class City
    {
        public int id { get; set; }
        public String name { get; set; }
        public int countryId { get; set; }
    }
}
