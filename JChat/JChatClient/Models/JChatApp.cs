﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace JChatClient.Models
{
    /// <summary>
    /// Class that represents the chat application´s model
    /// </summary>
    public class JChatApp
    {
        private static JChatApp _instance;
        public User profile = null;
        public Boolean isLoggedIn;
        public String contactsStatus = "all";
        public int ListBox1LastIndex;
        public List<User> searchList = null;
        public List<User> friendshipRequestsList = null;
        public List<Contact> contactsList = null;
        public List<Country> countriesList = null;
        public List<City> citiesList = null;
        public List<Gender> gendersList = null;
        public List<OnlineStatus> statusList = null;
        public List<Message> messagesList = null;

        protected JChatApp()
        {

        }

        // Singleton Pattern design
        public static JChatApp Instance()
        {
            if (_instance == null)
                _instance = new JChatApp();

            return _instance;
        }

        /// <summary>
        /// Updates the "friendshipRequestsList" variable for the incoming friendship requests
        /// </summary>
        /// <param name="tokenSource">Signal to cancel the update process</param>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed</returns>
        public String updateFriendshipRequests(CancellationTokenSource tokenSource)
        {
            while (true)
            {
                if (tokenSource.IsCancellationRequested)
                    return "ack";

                if (this.friendshipRequestsList == null)
                    this.friendshipRequestsList = new List<User>();

                this.friendshipRequestsList.Clear();

                // Requests to server manager
                Controllers.AppRequests requests = new Controllers.AppRequests();

                // Request genders information to server
                String friendshipRequestsResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlAllFriendshipRequests(Convert.ToString(this.profile.id))), true);

                if (friendshipRequestsResponse.Equals("nack"))
                    return "nack";

                XDocument xmlDocument = XDocument.Parse(friendshipRequestsResponse);

                IEnumerable<XElement> xmlUsers = xmlDocument.Element("frame").Element("body").Element("users").Elements("user");

                if (!xmlUsers.ElementAtOrDefault(0).Value.Trim().Equals("null"))
                {
                    User userRecord = null;

                    foreach (XElement user in xmlUsers)
                    {

                        userRecord = new User();

                        userRecord.id = Convert.ToInt32(user.Element("id").Value.Trim());
                        userRecord.email = user.Element("email").Value.Trim();
                        userRecord.nickname = user.Element("nickname").Value.Trim();
                        userRecord.firstName = user.Element("firstname").Value.Trim();
                        userRecord.lastName = user.Element("lastname").Value.Trim();
                        userRecord.birthdate = Convert.ToDateTime(user.Element("birthdate").Value.Trim());
                        userRecord.genderId = Convert.ToInt32(user.Element("gender_id").Value.Trim());
                        userRecord.enabled = (user.Element("enabled").Value.Trim().Equals("yes") ? true : false);
                        userRecord.onlineStatusId = Convert.ToInt32(user.Element("status_id").Value.Trim());
                        userRecord.cityId = (user.Element("city_id").Value.Trim().Equals("null") ? -1 : Convert.ToInt32(user.Element("city_id").Value.Trim()));

                        this.friendshipRequestsList.Add(userRecord);
                    }
                }
                Thread.Sleep(2000);
            }
        }

        /// <summary>
        /// Updates "gendersList" variable with all genders from server
        /// </summary>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed</returns>
        public String updateGendersFromServer()
        {
            if (this.gendersList == null)
                this.gendersList = new List<Gender>();

            this.gendersList.Clear();

            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request genders information to server
            String gendersResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlAllGenders()), true);

            if (gendersResponse.Equals("nack"))
                return "nack";

            XDocument xmlDocument = XDocument.Parse(gendersResponse);

            IEnumerable<XElement> allXmlGenders = xmlDocument.Element("frame").Element("body").Element("genders").Elements("gender");

            if (allXmlGenders.ElementAt(0).Value.Trim() == "null")
                return "ack";

            Gender genderRecord = null;

            foreach (XElement gender in allXmlGenders)
            {
                genderRecord = new Gender();
                genderRecord.id = Convert.ToInt32(gender.Element("id").Value.Trim());
                genderRecord.description = gender.Element("name").Value.Trim();

                this.gendersList.Add(genderRecord);
            }

            return "ack";
        }

        /// <summary>
        /// Updates "countriesList" variable with all countries from server
        /// </summary>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed</returns>
        public String updateCountriesFromServer()
        {

            if (this.countriesList == null)
                this.countriesList = new List<Country>();

            this.countriesList.Clear();

            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request genders information to server
            String countriesResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlAllCountries()), true);

            if (countriesResponse.Equals("nack"))
                return "nack";

            XDocument xmlDocument = XDocument.Parse(countriesResponse);

            IEnumerable<XElement> allXmlCountries = xmlDocument.Element("frame").Element("body").Element("countries").Elements("country");

            if (allXmlCountries.ElementAt(0).Value.Trim() == "null")
                return "ack";

            Country countryRecord = null;

            foreach (XElement country in allXmlCountries)
            {
                countryRecord = new Country();
                countryRecord.id = Convert.ToInt32(country.Element("id").Value.Trim());
                countryRecord.name = country.Element("name").Value.Trim();

                this.countriesList.Add(countryRecord);
            }

            return "ack";

        }

        /// <summary>
        /// Updates "citiesList" variable with all cities from server
        /// </summary>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed</returns>
        public String updateCitiesFromServer()
        {

            if (this.citiesList == null)
                this.citiesList = new List<City>();

            this.citiesList.Clear();

            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request genders information to server
            String citiesResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlAllCities()), true);

            if (citiesResponse.Equals("nack"))
                return "nack";

            XDocument xmlDocument = XDocument.Parse(citiesResponse);

            IEnumerable<XElement> allXmlCities = xmlDocument.Element("frame").Element("body").Element("cities").Elements("city");

            if (allXmlCities.ElementAt(0).Value.Trim() == "null")
                return "ack";

            City cityRecord = null;

            foreach (XElement city in allXmlCities)
            {
                cityRecord = new City();

                cityRecord.id = Convert.ToInt32(city.Element("id").Value.Trim());
                cityRecord.name = city.Element("name").Value.Trim();
                cityRecord.countryId = Convert.ToInt32(city.Element("country_id").Value.Trim());

                this.citiesList.Add(cityRecord);
            }

            return "ack";

        }


        /// <summary>
        /// Updates "statusList" variable with all genders from server
        /// </summary>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed</returns>
        public String updateStatusFromServer()
        {
            if (this.statusList == null)
                this.statusList = new List<OnlineStatus>();

            this.statusList.Clear();

            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request genders information to server
            String statusResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlAllOnlineStatus()), true);


            if (statusResponse.Equals("nack"))
                return "nack";

            XDocument xmlDocument = XDocument.Parse(statusResponse);

            IEnumerable<XElement> allXmlStatus = xmlDocument.Element("frame").Element("body").Element("all_status").Elements("status");

            if (allXmlStatus.ElementAt(0).Value.Trim() == "null")
                return "ack";

            OnlineStatus statusRecord = null;

            foreach (XElement status in allXmlStatus)
            {
                statusRecord = new OnlineStatus();

                statusRecord.id = Convert.ToInt32(status.Element("id").Value.Trim());
                statusRecord.descripton = status.Element("description").Value.Trim();

                this.statusList.Add(statusRecord);
            }

            return "ack";
        }


        /// <summary>
        /// Updates "messagesList" with all user´s conversations messages from server 
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed</returns>
        public String updateMessagesFromServer(int userId)
        {
            String usrId = Convert.ToString(userId);

            if (this.messagesList == null)
                this.messagesList = new List<Message>();

            this.messagesList.Clear();

            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request genders information to server
            String messagesResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlAllMessages(usrId)), true);

            if (messagesResponse.Equals("nack"))
                return "nack";

            XDocument xmlDocument = XDocument.Parse(messagesResponse);

            IEnumerable<XElement> allXmlMessages = xmlDocument.Element("frame").Element("body").Element("messages").Elements("message");

            if (allXmlMessages.ElementAt(0).Value.Trim() == "null")
                return "ack";

            Message messageRecord = null;

            foreach (XElement message in allXmlMessages)
            {
                messageRecord = new Message();

                messageRecord.id = Convert.ToInt32(message.Element("message_id").Value.Trim());
                messageRecord.senderId = Convert.ToInt32(message.Element("sender_id").Value.Trim());
                messageRecord.receiverId = Convert.ToInt32(message.Element("receiver_id").Value.Trim());
                messageRecord.message = message.Element("message_text").Value.Trim();
                messageRecord.dateTime = Convert.ToDateTime(message.Element("datetime").Value.Trim());

                this.messagesList.Add(messageRecord);
            }

            return "ack";
        }


        /// <summary>
        /// Updates "contactsList" variable with all user´s contacts
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed</returns>
        public String updateContactsFromServer(int userId)
        {
            String usrId = Convert.ToString(userId);

            if (this.contactsList == null)
                this.contactsList = new List<Contact>();

            this.contactsList.Clear();

            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request genders information to server
            String contactsResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlAllContacts(usrId)), true);

            if (contactsResponse.Equals("nack"))
                return "nack";

            XDocument xmlDocument = XDocument.Parse(contactsResponse);

            IEnumerable<XElement> allXmlContacts = xmlDocument.Element("frame").Element("body").Element("contacts").Elements("contact");

            if (allXmlContacts.ElementAt(0).Value.Trim() == "null")
                return "ack";

            Contact contactRecord = null;

            foreach (XElement contact in allXmlContacts)
            {
                contactRecord = new Contact();

                contactRecord.id = Convert.ToInt32(contact.Element("id").Value.Trim());
                contactRecord.email = contact.Element("email").Value.Trim();
                contactRecord.nickname = contact.Element("nickname").Value.Trim();
                contactRecord.firstName = contact.Element("firstname").Value.Trim();
                contactRecord.lastName = contact.Element("lastname").Value.Trim();
                contactRecord.blocked = (contact.Element("blocked").Value.Trim().Equals("yes") ? true : false);
                contactRecord.birthdate = Convert.ToDateTime(contact.Element("birthdate").Value.Trim());
                contactRecord.genderId = Convert.ToInt32(contact.Element("gender_id").Value.Trim());
                contactRecord.enabled = (contact.Element("enabled").Value.Trim().Equals("yes") ? true : false);
                contactRecord.logged = (contact.Element("logged").Value.Trim().Equals("yes") ? true : false);

                if (!contactRecord.logged)
                    contactRecord.onlineStatusId = 4;
                else contactRecord.onlineStatusId = Convert.ToInt32(contact.Element("status_id").Value.Trim());

                contactRecord.cityId = (contact.Element("city_id").Value.Trim().Equals("null") ? -1 : Convert.ToInt32(contact.Element("city_id").Value.Trim()));
                contactRecord.connectionDateTime = Convert.ToDateTime(contact.Element("connection_date").Value.Trim());

                this.contactsList.Add(contactRecord);
            }

            return "ack";
        }

        /// <summary>
        /// Updates "profile" with the user´s informatiom from server
        /// </summary>
        /// <param name="email">user´s email address</param>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed</returns>
        public String updateProfileFromServer(String email)
        {

            if (this.profile == null)
                this.profile = new User();

            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request genders information to server
            String profileResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlUserProfile(email)), true);

            if (profileResponse.Equals("nack"))
                return "nack";

            XDocument xmlDocument = XDocument.Parse(profileResponse);

            XElement xmlProfile = xmlDocument.Element("frame").Element("body").Element("profile");

            if (xmlProfile.Value.Trim() == "null" || xmlProfile.Value.Trim() == "error")
                return "ack";

            profile.id = Convert.ToInt32(xmlProfile.Element("id").Value.Trim());
            profile.email = xmlProfile.Element("email").Value.Trim();
            profile.nickname = xmlProfile.Element("nickname").Value.Trim();
            profile.firstName = xmlProfile.Element("firstname").Value.Trim();
            profile.lastName = xmlProfile.Element("lastname").Value.Trim();
            profile.birthdate = Convert.ToDateTime(xmlProfile.Element("birthdate").Value.Trim());
            profile.genderId = Convert.ToInt32(xmlProfile.Element("gender_id").Value.Trim());
            profile.enabled = (xmlProfile.Element("enabled").Value.Trim().Equals("yes") ? true : false);
            profile.logged = (xmlProfile.Element("logged").Value.Trim().Equals("yes") ? true : false);
            profile.onlineStatusId = Convert.ToInt32(xmlProfile.Element("status_id").Value.Trim());
            profile.cityId = (xmlProfile.Element("city_id").Value.Trim().Equals("null") ? -1 : Convert.ToInt32(xmlProfile.Element("city_id").Value.Trim()));

            return "ack";
        }

        /// <summary>
        /// Updates "profile.email" variable of the user
        /// </summary>
        /// <param name="email"></param>
        public void updateProfileEmail(String email)
        {
            if (this.profile == null)
                this.profile = new User();

            this.profile.email = email;
        }

        /// <summary>
        /// Updates "searchList" variable with all chat users searched by the current user
        /// </summary>
        /// <param name="searchResult">Search string</param>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed</returns>
        public String updateSearchFromServer(String searchResult)
        {
            if (searchResult.Equals("nack"))
                return "nack";

            if (this.searchList == null)
                this.searchList = new List<User>();

            this.searchList.Clear();

            XDocument xmlDocument = XDocument.Parse(searchResult);

            IEnumerable<XElement> xmlUsers = xmlDocument.Element("frame").Element("body").Element("users").Elements("user");

            if (xmlUsers.ElementAt(0).Value.Trim() == "null")
                return "ack";

            User userRecord = null;

            foreach (XElement user in xmlUsers)
            {
                int userId = Convert.ToInt32(user.Element("id").Value.Trim());

                if (this.contactsList.Where(c => c.id == userId).Count() == 0 && this.profile.id != userId)
                {

                    userRecord = new User();

                    userRecord.id = userId;
                    userRecord.email = user.Element("email").Value.Trim();
                    userRecord.nickname = user.Element("nickname").Value.Trim();
                    userRecord.firstName = user.Element("firstname").Value.Trim();
                    userRecord.lastName = user.Element("lastname").Value.Trim();
                    userRecord.birthdate = Convert.ToDateTime(user.Element("birthdate").Value.Trim());
                    userRecord.genderId = Convert.ToInt32(user.Element("gender_id").Value.Trim());
                    userRecord.enabled = (user.Element("enabled").Value.Trim().Equals("yes") ? true : false);
                    userRecord.onlineStatusId = Convert.ToInt32(user.Element("status_id").Value.Trim());
                    userRecord.cityId = (user.Element("city_id").Value.Trim().Equals("null") ? -1 : Convert.ToInt32(user.Element("city_id").Value.Trim()));

                    this.searchList.Add(userRecord);
                }
            }

            return "ack";
        }

        /// <summary>
        /// Tests if an email address already exists
        /// </summary>
        /// <param name="email">Email address</param>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed</returns>
        public String existsEmail(String email)
        {
            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request information to server if email exists
            String emailResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlIfExistEmail(email)), true);

            if (emailResponse.Equals("nack"))
                return "nack";

            XDocument xmlDocument = XDocument.Parse(emailResponse);

            XElement xmlEmail = xmlDocument.Element("frame").Element("body").Element("email");

            if (xmlEmail.Value.Trim().Equals("exists"))
                return "exist";
            else if (xmlEmail.Value.Trim().Equals("nexists") || xmlEmail.Value.Trim().Equals("error"))
                return "nexist";

            // Inihibits by default
            return "exist";
        }

        /// <summary>
        /// Creates a new user account
        /// </summary>
        /// <param name="chatName">New user´s Nickname</param>
        /// <param name="email">New user´s email address</param>
        /// <param name="password">New user´s password</param>
        /// <param name="firstName">New user´s first name</param>
        /// <param name="lastName">New user´s last name</param>
        /// <param name="birthday">New user´s birthday</param>
        /// <param name="gender_id">New user´s gender´s identification</param>
        /// <param name="country_id">New user´s country´s identification</param>
        /// <param name="city_id">New user´s city´s identification</param>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed; "ok" if new user has saved; "nok" if occurred an error</returns>
        public String saveNewUserResponse(String chatName, String email, String password, String firstName, String lastName, DateTime birthday, String gender_id, String country_id, String city_id)
        {
            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request cities of selected country information to server
            String newUserResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlSaveNewUser(chatName, email, password, firstName, lastName, birthday, gender_id, country_id, city_id)), true);

            if (newUserResponse.Equals("nack"))
                return "nack";

            XDocument xmlDocument = XDocument.Parse(newUserResponse);

            XElement response = xmlDocument.Element("frame").Element("body").Element("response");

            if (response.Value.Trim().Equals("ok"))
                return "ok";
            else if (response.Value.Trim().Equals("nok"))
                return "nok";

            return "nok";
        }

        /// <summary>
        /// Updates the user´s profile
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <param name="chatName">User´s nickname</param>
        /// <param name="password">User´s password</param>
        /// <param name="firstName">User´s first name</param>
        /// <param name="lastName">User´s last name</param>
        /// <param name="birthday">User´s birthday</param>
        /// <param name="gender_id">User´s gender´s identification</param>
        /// <param name="country_id">User´s country´s identification</param>
        /// <param name="city_id">User´s city´s identification</param>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed; "ok" if new user has saved; "nok" if occurred an error</returns>
        public String editUserProfileResponse(String userId, String chatName, String password, String firstName, String lastName, DateTime birthday, String gender_id, String country_id, String city_id)
        {
            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request cities of selected country information to server
            String newUserResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlEditUserProfile(userId, chatName, password, firstName, lastName, birthday, gender_id, country_id, city_id)), true);

            if (newUserResponse.Equals("nack"))
                return "nack";

            XDocument xmlDocument = XDocument.Parse(newUserResponse);

            XElement response = xmlDocument.Element("frame").Element("body").Element("response");

            if (response.Value.Trim().Equals("ok"))
                return "ok";
            else if (response.Value.Trim().Equals("nok"))
                return "nok";

            return "nok";
        }

        /// <summary>
        /// Authenticates/log in user
        /// </summary>
        /// <param name="email">User´s email address</param>
        /// <param name="password">User´s password</param>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed; "ok" if new user has saved; "nok" if occurred an error</returns>
        public String authenticateUser(String email, String password)
        {
            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request cities of selected country information to server
            String authenticationResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlAuthenticationUser(email, password)), true);

            if (authenticationResponse.Equals("nack"))
                return "nack";

            XDocument xmlDocument = XDocument.Parse(authenticationResponse);

            XElement response = xmlDocument.Element("frame").Element("body").Element("authentication");

            if (response.Value.Trim().Equals("ok"))
                return "ok";
            else if (response.Value.Trim().Equals("nok"))
                return "nok";

            return "nok";
        }

        /// <summary>
        /// User´s logout
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed</returns>
        public String userLogout(int userId)
        {
            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request genders information to server
            String logoutResponse = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlUserLogout(Convert.ToString(userId))), true);

            if (logoutResponse.Equals("nack"))
                return "nack";
            else return "ack";
        }

        /// <summary>
        /// Sends a message to other user
        /// </summary>
        /// <param name="senderId">Identification number from the user sending the message</param>
        /// <param name="receiverId">Identification number from the user receiving the message</param>
        /// <param name="message"></param>
        /// <returns>"ack" if connection with server has successful; "nack" if if connection with server has failed;</returns>
        public String sendMessageToUser(String senderId, String receiverId, String message)
        {
            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request cities of selected country information to server
            String response = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlSendMessage(senderId, receiverId, message)), false);

            if (response.Equals("nack"))
                return "nack";

            response = this.updateMessagesFromServer(this.profile.id);

            if (response.Equals("nack"))
                return "nack";
            else return "ack";
        }

        /// <summary>
        /// Search other users
        /// </summary>
        /// <param name="search">word pattern for search</param>
        /// <returns>"ack" if connection with server was successful; "nack" if if connection with server has failed;</returns>
        public String searchUser(String search)
        {
            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request cities of selected country information to server
            String searchResult = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlSearchUser(search)), true);

            if (searchResult.Equals("nack"))
                return "nack";

            String response = this.updateSearchFromServer(searchResult);

            if (response.Equals("nack"))
                return "nack";
            else return "ack";
        }

        /// <summary>
        /// Performs a frindship request
        /// </summary>
        /// <param name="requesterId">Identification of the user requesting the friendship</param>
        /// <param name="requestedId">Identification of the user requested for friendship</param>
        /// <returns>"ack" if connection with server was successful; "nack" if if connection with server has failed;</returns>
        public String requestFriendship(String requesterId, String requestedId)
        {
            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request cities of selected country information to server
            String response = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlRequestFriendship(requesterId, requestedId)), false);

            if (response.Equals("nack"))
                return "nack";
            else return "ack";
        }

        /// <summary>
        /// Accepts a friendship request
        /// </summary>
        /// <param name="requesterId">Identification of the user requesting the friendship</param>
        /// <param name="requestedId">Identification of the user that received the requested the friendship</param>
        /// <returns>"ack" if connection with server was successful; "nack" if if connection with server has failed;</returns>
        public String requestFriendshipAcceptance(String requesterId, String requestedId)
        {
            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request cities of selected country information to server
            String response = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXMlRequestFriendshipAcceptance(requesterId, requestedId)), false);

            if (response.Equals("nack"))
                return "nack";
            else return "ack";
        }

        /// <summary>
        /// Request to change current user´s online status
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <param name="statusId">User´s new online status identification number</param>
        /// <returns>"ack" if connection with server was successful; "nack" if if connection with server has failed;</returns>
        public String changeOnlineStatus(String userId, String statusId)
        {
            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request cities of selected country information to server
            String response = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlChangeOnlineStatus(userId, statusId)), false);

            if (response.Equals("nack"))
                return "nack";
            else return "ack";
        }

        /// <summary>
        /// Deletes a contact from friends´ contacts
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <param name="contactId">Contact´s identification to delete</param>
        /// <returns>"ack" if connection with server was successful; "nack" if if connection with server has failed;</returns>
        public String deleteContact(String userId, String contactId)
        {
            // Requests to server manager
            Controllers.AppRequests requests = new Controllers.AppRequests();

            // Request cities of selected country information to server
            String response = requests.sendRequestToServer(GetLocalIPAddress(), 11000, Convert.ToString(this.getXmlDeleteContact(userId, contactId)), false);

            if (response.Equals("nack"))
                return "nack";
            else return "ack";
        }

        /* XML MESSAGES FOR THE REQUESTS TO SERVER */

        /// <summary>
        /// XML MESSAGE´S DOCUMENT TEMPLATE FOR ALL XML MESSAGES
        /// </summary>
        /// <param name="messageType">Header´s Type o message</param>
        /// <param name="sender">Header´s identification neumber of the user sending the message</param>
        /// <param name="receiver">Header´s identification number of the user receiving the message</param>
        /// <param name="body">Information message</param>
        /// <returns>XML message</returns>
        private XDocument getXmlDocument(string messageType, String sender, String receiver, XElement body)
        {
            XDocument xmlDoc = new XDocument(new XDeclaration("2.0", "UTF-8", "yes"));
            XElement frame = new XElement("frame");

            XElement header = new XElement("header");
            header.Add(new XElement("message_type", messageType));

            if (sender != String.Empty)
                header.Add(new XElement("sender", sender));

            if (receiver != String.Empty)
                header.Add(new XElement("receiver", receiver));

            XElement bodyMessage = new XElement("body");
            bodyMessage.Add(body);

            frame.Add(header);

            if (body != null)
                frame.Add(bodyMessage);

            xmlDoc.Add(frame);

            return xmlDoc;
        }

        /// <summary>
        /// XML message to get all genders from server
        /// </summary>
        /// <returns>XML message</returns>
        private XDocument getXmlAllGenders()
        {
            return getXmlDocument("ALL_GENDERS", "", "", null);
        }

        /// <summary>
        /// XML message to get all countries from server
        /// </summary>
        /// <returns>XML messsage</returns>
        private XDocument getXmlAllCountries()
        {
            return this.getXmlDocument("ALL_COUNTRIES", "", "", null);
        }

        /// <summary>
        /// XML message to get all cities from server
        /// </summary>
        /// <returns>XML message</returns>
        private XDocument getXmlAllCities()
        {
            return this.getXmlDocument("ALL_CITIES", "", "", null);
        }

        /// <summary>
        /// XML message to test if an email address already exists
        /// </summary>
        /// <param name="email">Email address</param>
        /// <returns>XML message</returns>
        private XDocument getXmlIfExistEmail(String email)
        {
            XElement arguments = new XElement("arguments",
                                              new XElement("email", email));

            return this.getXmlDocument("EXISTS_EMAIL", "", "", arguments);
        }

        /// <summary>
        /// XML message to get all messages of all conversations of the user from server
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>XML message</returns>
        private XDocument getXmlAllMessages(String userId)
        {
            XElement arguments = new XElement("arguments",
                                              new XElement("user_one", userId),
                                              new XElement("user_two", "null"));

            return this.getXmlDocument("ALL_MESSAGES_BETWEEN_USERS", "", "", arguments);
        }

        /// <summary>
        /// XML message to get all online status
        /// </summary>
        /// <returns>XML message</returns>
        private XDocument getXmlAllOnlineStatus()
        {
            return this.getXmlDocument("ALL_STATUS", "", "", null);
        }

        /// <summary>
        /// XML message to get all user´s contacts
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>XML message</returns>
        private XDocument getXmlAllContacts(String userId)
        {
            XElement arguments = new XElement("arguments",
                                              new XElement("user_id", userId));

            return this.getXmlDocument("USER_CONTACTS", "", "", arguments);
        }

        /// <summary>
        /// XML message to get user´s profile information
        /// </summary>
        /// <param name="email">User´s email address</param>
        /// <returns>XML message</returns>
        private XDocument getXmlUserProfile(String email)
        {
            XElement arguments = new XElement("arguments",
                                              new XElement("email", email));

            return this.getXmlDocument("USER_PROFILE", "", "", arguments);
        }

        /// <summary>
        /// XML message to get authenticate the user
        /// </summary>
        /// <param name="email">User´s email address</param>
        /// <param name="password">User´s password</param>
        /// <returns>XML message</returns>
        private XDocument getXmlAuthenticationUser(String email, String password)
        {
            XElement arguments = new XElement("arguments",
                                              new XElement("email", email),
                                              new XElement("password", password));

            return this.getXmlDocument("USER_AUTHENTICATION", "", "", arguments);
        }


        /// <summary>
        /// XML message to logout the user
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>XML message</returns>
        private XDocument getXmlUserLogout(String userId)
        {
            XElement arguments = new XElement("arguments",
                                              new XElement("user_id", userId));

            return this.getXmlDocument("USER_LOGOUT", "", "", arguments);
        }

        /// <summary>
        /// XML message to send a message to another user
        /// </summary>
        /// <param name="senderId">Identification number of the user sending the message</param>
        /// <param name="receiverId">Identification number of the user receiving the message</param>
        /// <param name="message">Message to send</param>
        /// <returns>XML message</returns>
        private XDocument getXmlSendMessage(String senderId, String receiverId, String message)
        {
            XElement arguments = new XElement("communication",
                                              new XElement("message", message),
                                              new XElement("date_time", DateTime.Now.ToString()));

            return this.getXmlDocument("USER_MESSAGE", senderId, receiverId, arguments);
        }

        /// <summary>
        /// XML message to make a search about other users
        /// </summary>
        /// <param name="search">Word pattern for the search</param>
        /// <returns>XML message</returns>
        private XDocument getXmlSearchUser(String search)
        {
            XElement arguments = new XElement("arguments",
                                              new XElement("search", search));

            return this.getXmlDocument("USER_SEARCH", "", "", arguments);
        }

        /// <summary>
        /// XML message to create new user account
        /// </summary>
        /// <param name="chatName">New user´s nickame</param>
        /// <param name="email">New user´s email address</param>
        /// <param name="password">New user´s password</param>
        /// <param name="firstName">New user´s first name</param>
        /// <param name="lastName">New user´s last name</param>
        /// <param name="birthDate">New user´s birthday</param>
        /// <param name="gender_id">New user´s gender´s identification number</param>
        /// <param name="country_id">New user´s country´s identification number</param>
        /// <param name="city_id">New user´s city´s identification number</param>
        /// <returns>XML message</returns>
        private XDocument getXmlSaveNewUser(String chatName, String email, String password, String firstName, String lastName, DateTime birthDate, String gender_id, String country_id, String city_id)
        {
            XElement user = new XElement("user",
                                         new XElement("chat_name", chatName),
                                         new XElement("email", email),
                                         new XElement("password", password),
                                         new XElement("first_name", firstName),
                                         new XElement("last_name", lastName),
                                         new XElement("birthday", birthDate.ToString()),
                                         new XElement("gender_id", gender_id),
                                         new XElement("city_id", city_id));

            return this.getXmlDocument("NEW_USER_ACCOUNT", "", "", user);
        }

        /// <summary>
        /// XML message to update user´s profile information
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <param name="chatName">User´s nickname</param>
        /// <param name="password">User´s password</param>
        /// <param name="firstName">User´s first name</param>
        /// <param name="lastName">User´s last name</param>
        /// <param name="birthDate">User´s birthday</param>
        /// <param name="gender_id">User´s gender´s identification number</param>
        /// <param name="country_id">User´s country´s identification number</param>
        /// <param name="city_id">User´s city´s identification number</param>
        /// <returns>XML message</returns>
        private XDocument getXmlEditUserProfile(String userId, String chatName, String password, String firstName, String lastName, DateTime birthDate, String gender_id, String country_id, String city_id)
        {
            XElement user = new XElement("user",
                                         new XElement("user_id", userId),
                                         new XElement("chat_name", chatName),
                                         new XElement("password", password),
                                         new XElement("first_name", firstName),
                                         new XElement("last_name", lastName),
                                         new XElement("birthday", birthDate.ToString()),
                                         new XElement("gender_id", gender_id),
                                         new XElement("city_id", city_id));

            return this.getXmlDocument("EDIT_USER_PROFILE", "", "", user);
        }

        /// <summary>
        /// XML message to request friendship
        /// </summary>
        /// <param name="requesterId">Identification number of the user requesting friendship</param>
        /// <param name="requestedId">Identification number of the user receiving the request for friendship</param>
        /// <returns>XML message</returns>
        private XDocument getXmlRequestFriendship(String requesterId, String requestedId)
        {
            return this.getXmlDocument("FRIENDSHIP_REQUEST", requesterId, requestedId, null);
        }

        /// <summary>
        /// XML message to accept a friendship request
        /// </summary>
        /// <param name="requesterId">Identification number of the user requesting the friendship</param>
        /// <param name="requestedId">Identification number of the user receiving the friendship request</param>
        /// <returns>XML message</returns>
        private XDocument getXMlRequestFriendshipAcceptance(String requesterId, String requestedId)
        {
            return this.getXmlDocument("FRIENDSHIP_REQUEST_ACCEPTANCE", requesterId, requestedId, null);
        }

        /// <summary>
        /// XML message to get all friendship requests received
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <returns>XML message</returns>
        private XDocument getXmlAllFriendshipRequests(String userId)
        {
            XElement arguments = new XElement("arguments",
                                              new XElement("user_id", userId));

            return this.getXmlDocument("USER_FRIENDSHIP_REQUESTS", "", "", arguments);
        }

        /// <summary>
        /// XML message to update user´s online status
        /// </summary>
        /// <param name="userId">User´´s identification number</param>
        /// <param name="statusId">New online status´ identification number</param>
        /// <returns>XML message</returns>
        private XDocument getXmlChangeOnlineStatus(String userId, String statusId)
        {
            XElement arguments = new XElement("arguments",
                                              new XElement("status_id", statusId));

            return this.getXmlDocument("USER_STATUS_CHANGE", userId, "", arguments);
        }

        /// <summary>
        /// XML message to delete an user´s contact
        /// </summary>
        /// <param name="userId">User´s identification number</param>
        /// <param name="contactId">Contact´s identification number to delete</param>
        /// <returns>XML message</returns>
        private XDocument getXmlDeleteContact(String userId, String contactId)
        {
            return this.getXmlDocument("DELETE_CONTACT", userId, contactId, null);
        }

        /* Methods to access information */

        /// <summary>
        /// Retrives all id/value pairs with all the genders
        /// </summary>
        /// <returns>Dictionary</returns>
        public Dictionary<String, String> getAllGendersData()
        {
            Dictionary<String, String> allGenders = new Dictionary<String, String>();

            if (this.gendersList.Count() == 0)
            {
                allGenders.Add("-1", "No genders...");
                return allGenders;
            }

            foreach (Gender gender in this.gendersList)
                allGenders.Add(Convert.ToString(gender.id), gender.description);

            return allGenders;
        }

        /// <summary>
        /// Retrives all id/value pairs with all the countries
        /// </summary>
        /// <returns>Dictionary object</returns>
        public Dictionary<String, String> getAllCountriesData()
        {
            Dictionary<String, String> allCountries = new Dictionary<String, String>();

            if (this.countriesList.Count() == 0)
            {
                allCountries.Add("-1", "No countries...");
                return allCountries;
            }

            foreach (Country country in this.countriesList)
                allCountries.Add(Convert.ToString(country.id), country.name);

            return allCountries;
        }

        /// <summary>
        /// Retrieves all id/values pairs with all cities from a country
        /// </summary>
        /// <param name="countryId">Country´s identification number</param>
        /// <returns>Dictionary object</returns>
        public Dictionary<String, String> getAllCitiesByCountryIdData(int countryId)
        {
            Dictionary<String, String> allCities = new Dictionary<String, String>();

            List<City> citiesOfCountry = this.citiesList.Where(ct => ct.countryId == countryId).ToList();

            if (citiesOfCountry.Count() == 0)
            {
                allCities.Add("-1", "No cities...");
                return allCities;
            }

            foreach (City country in citiesOfCountry)
                allCities.Add(Convert.ToString(country.id), country.name);

            return allCities;
        }

        /// <summary>
        /// Retrives contry´s identification number by city´s identification number
        /// </summary>
        /// <param name="cityId">City´s identification number</param>
        /// <returns>Country´s identification number</returns>
        public int getCountryIdByCityId(int cityId)
        {
            if (cityId == -1)
                return 0;

            City city = this.citiesList.Where(ct => ct.id == cityId).FirstOrDefault();

            return city.countryId;
        }

        /// <summary>
        /// Retrives City´s name/Country´s name pair by city´s identification number
        /// </summary>
        /// <param name="cityId">City´s identification number</param>
        /// <returns>String array - postion 0 => city´s name, position 1 => country´s name</returns>
        public String[] cityAndCountry(int cityId)
        {
            String[] cityAndCountry = new String[2];

            cityAndCountry[0] = this.citiesList.Where(ct => ct.id == cityId).FirstOrDefault().name;
            cityAndCountry[1] = this.countriesList.Where(ctr => ctr.id == this.citiesList.Where(ct => ct.id == cityId).FirstOrDefault().countryId).FirstOrDefault().name;

            return cityAndCountry;
        }

        /// <summary>
        /// Retrieves all id/value pairs with all online status
        /// </summary>
        /// <returns>Dictionary object</returns>
        public Dictionary<String, String> getAllOnlineStatusData()
        {
            Dictionary<String, String> allStatus = new Dictionary<String, String>();

            foreach (OnlineStatus status in this.statusList)
                allStatus.Add(Convert.ToString(status.id), status.descripton);

            return allStatus;
        }

        /// <summary>
        /// Retrieves user´s online status description
        /// </summary>
        /// <returns>User´s online status description</returns>
        public String getUserOnlineStatusData()
        {
            return this.statusList.Where(s => s.id == this.profile.onlineStatusId).FirstOrDefault().descripton;
        }

        /// <summary>
        /// Retrieves all id/value pairs with all user´s contacts
        /// </summary>
        /// <param name="searchMode">Word pattern search</param>
        /// <param name="status">"all" to retrive all users; "online" to retrieve only users that are online</param>
        /// <returns>Dictionary</returns>
        public Dictionary<String, String> getAllUserContactsData(Boolean searchMode, String status = "all")
        {
            Dictionary<String, String> allContacts = new Dictionary<String, String>();

            if (this.contactsList != null && this.contactsList.Count() > 0)
                foreach (Contact contact in this.contactsList)
                {
                    if (status.Equals("all"))
                        allContacts.Add(Convert.ToString(contact.id), contact.firstName + " " + contact.lastName + " (" + this.getContactOnlineStatusData(contact.id, searchMode) + ")");
                    else if (status.Equals("online") && contact.onlineStatusId != 3 && contact.onlineStatusId != 4)
                        allContacts.Add(Convert.ToString(contact.id), contact.firstName + " " + contact.lastName + " (" + this.getContactOnlineStatusData(contact.id, searchMode) + ")");
                }

            if (allContacts.Count() == 0)
                allContacts.Add("-1", "No contacts...");

            return allContacts;
        }

        /// <summary>
        /// Retrieves all users´ information that requested friendship
        /// </summary>
        /// <returns>DataTable object</returns>
        public DataTable getAllFriendshipRequestsData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id");
            dt.Columns.Add("Username");
            dt.Columns.Add("Gender");
            dt.Columns.Add("City");
            dt.Columns.Add("Country");

            foreach (User user in friendshipRequestsList)
            {
                String[] values = new String[5];

                values[0] = Convert.ToString(user.id);
                values[1] = user.firstName + " " + user.lastName;
                values[2] = this.gendersList.Where(g => g.id == user.genderId).FirstOrDefault().description;
                values[3] = (user.cityId != -1 ? this.citiesList.Where(c => c.id == user.cityId).FirstOrDefault().name : "No city...");
                values[4] = (user.cityId != -1 ? this.countriesList.Where(c => c.id == this.citiesList.Where(cr => cr.id == user.cityId).FirstOrDefault().countryId).FirstOrDefault().name : "No country...");

                dt.Rows.Add(values);
            }

            return dt;
        }

        /// <summary>
        /// Retrieves contact´s name
        /// </summary>
        /// <param name="userId">Contact´s identification number</param>
        /// <param name="searchMode">Search mode - "False" if is a user´s contact; "True" if is a search contact</param>
        /// <returns>Contact´s name</returns>
        public String getContactNameData(int userId, Boolean searchMode)
        {
            if (searchMode)
                return "";

            if (this.contactsList.Count() == 0)
                return "No contacts...";

            return this.contactsList.Where(c => c.id == userId).FirstOrDefault().firstName + " " + this.contactsList.Where(c => c.id == userId).FirstOrDefault().lastName;
        }

        /// <summary>
        /// Retrieves contact´s online status´ description
        /// </summary>
        /// <param name="userId">Contact´s identification number</param>
        /// <param name="searchMode">Search mode - "False" if is a user´s contact; "True" if is a search contact</param>
        /// <returns>Contact´s online status´ description</returns>
        public String getContactOnlineStatusData(int userId, Boolean searchMode)
        {
            if (searchMode)
                return "";

            if (this.contactsList.Count() == 0)
                return "No online status...";

            int statusId = this.contactsList.Where(c => c.id == userId).FirstOrDefault().onlineStatusId;

            if (statusId == 3 || statusId == 4)
                return "Offline";

            return this.statusList.Where(s => s.id == statusId).FirstOrDefault().descripton;
        }

        /// <summary>
        /// Retrieves all users from a search result
        /// </summary>
        /// <returns>Dictionary object</returns>
        public Dictionary<String, String> getSearchData()
        {
            Dictionary<String, String> users = new Dictionary<String, String>();

            if (this.searchList.Count() != 0)
            {
                foreach (User user in this.searchList)
                {

                    String username = user.firstName + " " + user.lastName + (user.cityId == -1 ? "" : " \nfrom " + this.cityAndCountry(user.cityId)[0] + ", " + this.cityAndCountry(user.cityId)[1]);
                    users.Add(Convert.ToString(user.id), username);
                }
            }
            else users.Add("-1", "No match found...");

            return users;
        }

        /// <summary>
        /// Retrieves whole conversation between user and contact
        /// </summary>
        /// <param name="contactId">Contact´s identification number</param>
        /// <returns>List of Message objects</returns>
        public List<Message> getConversationData(int contactId)
        {
            List<Message> conversation = this.messagesList.Where(m => m.senderId == contactId || m.receiverId == contactId).OrderBy(m => m.dateTime).ToList();

            return conversation;
        }

        /// <summary>
        /// Sets user´s online status
        /// </summary>
        /// <param name="statusId">New online status´ identification number</param>
        public void setOnlineStatusData(int statusId)
        {
            this.profile.onlineStatusId = statusId;
        }

        /// <summary>
        /// Adds new message to the "messageList" variable
        /// </summary>
        /// <param name="idMessage">Message´s identification number</param>
        /// <param name="senderId">Identification number of the user sending the message</param>
        /// <param name="receiverId">Identification number of the user receiving the message</param>
        /// <param name="message">Message</param>
        /// <param name="dateTime">Date of the message</param>
        public void addNewMessage(int idMessage, int senderId, int receiverId, String message, DateTime dateTime)
        {
            this.messagesList.Add(new Message()
            {
                id = idMessage,
                senderId = senderId,
                receiverId = receiverId,
                dateTime = dateTime
            });
        }

        /* Auxiliar */

        /// <summary>
        /// Gets local ip address
        /// </summary>
        /// <returns>Ip address</returns>
        private static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");

            //return "192.168.1.34";
        }
    }
}
