﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JChatClient.Models
{
    /// <summary>
    /// Model for a user´s online status
    /// </summary>
    public class OnlineStatus
    {
        public int id { get; set; }
        public String descripton { get; set; }
    }
}
