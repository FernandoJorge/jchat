﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JChatClient.Models
{
    /// <summary>
    /// Model for user´s information
    /// </summary>
    public class User
    {
        public int id { get; set; }
        public String nickname { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public String email { get; set; }
        public DateTime birthdate { get; set; }
        public int genderId { get; set; }
        public int onlineStatusId { get; set; }
        public Boolean enabled { get; set; }
        public Boolean logged { get; set; }
        public int cityId { get; set; }

        public User()
        {

        }

        public User(int id, String nickname, String firstName, String lastName, String email, DateTime birthdate, int genderId, int onlineStatusId, Boolean enabled)
        {
            this.id = id;
            this.nickname = nickname;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.birthdate = birthdate;
            this.genderId = genderId;
            this.onlineStatusId = onlineStatusId;
            this.enabled = enabled;
        }
    }
}
