﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JChatClient.Models
{
    /// <summary>
    /// Model for a user´s message
    /// </summary>
    public class Message
    {
        public int id { get; set; }
        public int senderId { get; set; }
        public int receiverId { get; set; }
        public DateTime dateTime { get; set; }
        public String message { get; set; }

    }
}
