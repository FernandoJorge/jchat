﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JChatClient.Models
{
    /// <summary>
    /// Model for a user´s contact
    /// </summary>
    public class Contact: User
    {
        public DateTime connectionDateTime { get; set; }
        public Boolean blocked { get; set; }

        //public Contact(int id, String nickname, String firstName, String lastName, String email, DateTime birthdate, int genderId, int onlineStatusId, Boolean enabled)
        //    : base(id, nickname, firstName, lastName, email, birthdate, genderId, onlineStatusId, enabled)
        //{

        //}
    }
}
