﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JChatClient.Models
{
    /// <summary>
    /// Model for a user´s gender
    /// </summary>
    public class Gender
    {
        public int id { get; set; }
        public String description { get; set; }
    }
}
